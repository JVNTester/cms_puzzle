<!DOCTYPE html>
<html>
<head>
    <title>title</title>
    <meta content="text/html; charset=Windows-1251" http-equiv="content-type">
    <meta name="keywords" value="<?php echo $keywords; ?>">
    <meta name="description" value="<?php echo $description; ?>">
    <?php
        foreach($styles as $style){
           echo '<link rel="stylesheet" type="text/css" media="screen" href="/'.CSS_DIR. '/' . $style . '.css">';
        }
    ?>

</head>
<body>
<div class="wrapper">
    <header>
        <div class="logo">
                <a href="/"><img src="/<?php echo ALL_IMG_DIR; ?>logo.png" alt="logo"></a>
            </div>
        <nav>
            <menu>
                <?php
                    foreach($topMenu as $item){
                        if($item['is_active']){
                            echo '<li><a href="/'.$item['full_cache_url'].'" class="active">'.$item['title_in_menu'].'</a></li>';
                        }else{
                            echo '<li><a href="/'.$item['full_cache_url'].'">'.$item['title_in_menu'].'</a></li>';
                        }
                    }
                ?>
            </menu>
        </nav>
    </header>
    <?php echo $content; ?>
    <footer>
        <div class="footer">
            <div class="logo">
                <img src="/<?php echo ALL_IMG_DIR ?>/logo.png" alt="logo">
            </div>
            <div class="footer__blocks">
                <div class="block">
                    <div class="block__icon"><img src="/<?php echo ALL_IMG_DIR ?>/phone.png" alt=""></div>
                    <div class="block__title">talk to us</div>
                    <div class="block__value"><?php echo $contact_inf['phone']; ?></div>
                </div>
                <div class="block">
                    <div class="block__icon"><img src="/<?php echo ALL_IMG_DIR ?>/mail.png" alt=""></div>
                    <div class="block__title">e-mail</div>
                    <div class="block__value"><?php echo $contact_inf['email']; ?></div>
                </div>
                <div class="block">
                    <div class="block__icon"><img src="/<?php echo ALL_IMG_DIR ?>/contact.png" alt=""></div>
                    <div class="block__title">location</div>
                    <div class="block__value"><?php echo $contact_inf['location']; ?></div>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php
    foreach($scripts as $script){
        echo '<script src="/'.JS_DIR. '/' . $script . '.js"></script>';
    }
    ?>
</body>
</html>
