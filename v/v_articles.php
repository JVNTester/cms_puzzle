<?php echo $gallery; ?>
<div class="blog articles_block__content">
	<div class="posts_wrap">
		<div class="posts">
		<?php
			foreach($page['content'] as $article){

				echo '<div class="blog_post">';
				echo '	<div class="blog_post_image">';
				echo '		<a href="/articles/'.$article['url'].'"><img src="/'.ARTICLES_IMG_DIR.$article['image'].'" alt=""></a>';
				echo '	</div>';
				echo '	<div class="blog_post_content">';
				echo '		<div class="blog_post_title"><a href="/articles/'.$article['url'].'">'.$article['title'].'</a></div>';
				echo '		<div class="blog_post_text">';
				echo '			<p>'.$article['description'].'...</p>';
				echo '		</div>';
				echo '		<div class="button blog_post_button"><a href="/articles/'.$article['url'].'">Подробнее</a></div>';
				echo '	</div>';
				echo '</div>';
			}
		?> 
		</div>
	</div>
	<!-- Sidebar -->
	<div class="sidebar">
		
		<!-- Search -->
		<div class="sidebar_search">
			<div class="sidebar_title">Поиск</div>
			<form action="/search/" class="sidebar_search_form" method="POST">
				<input type="text" class="sidebar_search_input" placeholder="Что ищем?" required="required" name="query">
				<input type="submit" class="sidebar_search_button" name="search" value="Поиск">
			</form>
		</div>

		<?php if($categories):?>
			<div class="categories">
				<div class="sidebar_title">Категории</div>
				<div class="sidebar_list">
					<ul>
						<?php foreach($categories as $category):?>
							<li><a href="search/<?php echo $category['title']; ?>"><?php echo $category['title']; ?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
		<?php endif;?>

		<?php if($left_menu):?>
		<div class="categories">
			<div class="sidebar_title">Страницы</div>
			<div class="sidebar_list">
				<ul>
					<?php foreach($left_menu as $item):?>
						<li><a href="/<?php echo $item['full_cache_url'];?>"><?php echo $item['title_in_menu'];?></a></li>
					<?php endforeach;?>
				</ul>
			</div>
		</div>
		<?php endif; ?>

	</div>
</div>
