
<div id="usersettings">
    <h1>Редактирование контактной информации</h1>
    <form method="post">
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Phone</label>
            <input type="text" class="form_field" placeholder="phone" name="phone" value="<?php echo $data['phone']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Email</label>
            <input type="text" class="form_field" placeholder="email" name="email" value="<?php echo $data['email']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Location</label>
            <input type="text" class="form_field" placeholder="location" name="location" value="<?php echo $data['location']; ?>">
        </div>

        <input type="submit" value="Сохранить изменения" class="btn btn-danger" name="save_data">
    </form>
</div>