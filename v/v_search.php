<div class="main_image">
	<img src="/<?php echo ALL_IMG_DIR; ?>/search_top.png" alt="">
</div>
<section class="search_page">
	<div class="extended_search">
		<div class="title">Расширенный поиск</div>
		<form method="POST">
			<div>
				<input type="text" name="title" placeholder="Заголовок статьи" value="<?php echo $fields['title']; ?>">
				<select name="category">
					<option value="0">Категория не выбрана</option>
					<?php
						foreach($categories as $category){
							if($fields['category'] == $category['id_category']){
								echo '<option value="'.$category['id_category'].'" selected>'.$category['title'].'</option>';
							}else{
								echo '<option value="'.$category['id_category'].'">'.$category['title'].'</option>';
							}
							
						}
					?>
				</select>
			</div>
			<div>
				<input type="text" id="datepicker" name="dateFrom" placeholder="Публикация от..." value="<?php echo $fields['dateFrom']; ?>">
				<input type="text" id="datepicker2" name="dateTo" placeholder="Публикация до..." value="<?php echo $fields['dateTo']; ?>">
			</div>
			<div>
				<input type="submit" value="Найти" name="filter">
				<input type="submit" value="Сбросить фильтры" name="clearFilters">
			</div>
		</form>
	</div>

		<?php if($is_search):?>
			<?php if($search):?>
			
					<div class="posts">
						<?php
							foreach($search as $article){

								echo '<div class="blog_post">';
								echo '	<div class="blog_post_image">';
								echo '		<a href="/articles/'.$article['url'].'"><img src="/'.ARTICLES_IMG_DIR.$article['image'].'" alt=""></a>';
								echo '	</div>';
								echo '	<div class="blog_post_content">';
								echo '		<div class="blog_post_title"><a href="/articles/'.$article['url'].'">'.$article['title'].'</a></div>';
								echo '		<div class="blog_post_text">';
								echo '			<p>'.$article['description'].'...</p>';
								echo '		</div>';
								echo '		<div class="button blog_post_button"><a href="/articles/'.$article['url'].'">Подробнее</a></div>';
								echo '	</div>';
								echo '</div>';
							}
						?> 
					</div>
			<?php else:?>
				<p>Ничего не найдено</p>
			<?php endif;?>
		<?php endif;?>

		<?php if($is_filter):?>
			<?php if($filter):?>
				<div class="posts">
					<?php
						foreach($filter as $article){

							echo '<div class="blog_post">';
							echo '	<div class="blog_post_image">';
							echo '		<a href="/articles/'.$article['url'].'"><img src="/'.ARTICLES_IMG_DIR.$article['image'].'" alt=""></a>';
							echo '	</div>';
							echo '	<div class="blog_post_content">';
							echo '		<div class="blog_post_title"><a href="/articles/'.$article['url'].'">'.$article['title'].'</a></div>';
							echo '		<div class="blog_post_text">';
							echo '			<p>'.$article['description'].'...</p>';
							echo '		</div>';
							echo '		<div class="button blog_post_button"><a href="/articles/'.$article['url'].'">Подробнее</a></div>';
							echo '	</div>';
							echo '</div>';
						}
					?> 
				</div>
			<?php else:?>
			<p>Ничего не найдено</p>
		<?php endif;?>
	<?php endif;?>
</section>