<div class="slider">
	<div class="owl-carousel owl-theme">
		    <?php
		        foreach($gallery as $image){
					echo '<div class="slide" style="background: url(/'.IMG_DIR . $image['path'].'); background-size: cover;">';
					echo '<div class="img" ></div>';
					echo '<div class="slide__text"><span>never</span> limit yourself</div>';
					echo '</div>';
		        }
		    ?>
	</div>
</div>