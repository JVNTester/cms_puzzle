<?php echo $gallery; ?>
<section class="about contacts">
	<div class="about_left ">
		<div class="logo">
			<img src="/<?php echo ALL_IMG_DIR;?>logo.png" alt="logo">
		</div>
		<div class="about_left__title">Our Contact Info</div>
		<div class="content_block">
			<div class="content_block__left">
				<div class="content_block__left__text"><?php echo $footer_fields['description']; ?></div>
			</div>
			<div class="content_block__right">
				<div class="list">
					<ul>
						<li><?php echo $footer_fields['location']; ?></li>
						<li><?php echo $footer_fields['phone']; ?></li>
						<li><?php echo $footer_fields['email']; ?></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="form">
			<form method="post">
				<div>
					<input type="text" name="name" required placeholder="Your Name">
					<input type="email" name="email" required placeholder="Your Email">
				</div>
				<textarea name="message" placeholder="Message"></textarea>
				<input type="submit" value="Отправить сообщение" name="contact_form">
			</form>
		</div>
	</div>
	<div class="about_right">
		<div class="map">
			<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Abad0284a370d266525f7757c9826be7625bd5300b240afdb296763dd4766d7c3&amp;width=700&amp;height=720&amp;lang=ru_RU&amp;scroll=true"></script>
		</div>
	</div>
</section>