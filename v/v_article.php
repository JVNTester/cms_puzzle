<div class="main_image">
	<img src="/<?php echo ARTICLES_IMG_DIR.$page['image']; ?>" alt="">
</div>
<div class="blog article_block">
	<div class="posts_wrap article_block__content">
		<div class="content_article">
			<h1><?php echo $page['title']?></h1>
			<div class="posts_wrap__content"><?php echo $page['content'];?></div>
			<div class="date">Дата публикации: <?php echo date('d-m-Y', strtotime($page['date'])); ?></div>
			<div class="autor">Автор: <?php echo $page['autor'];?></div>

			<div class="comments">
				<form method="POST" class="comment">
					<?php 
						if($message){
							echo '<div class="btn btn-info">'.$message.'</div>';
						}
					?>
					 <div class="wrapper_field wrapper_field-input">
			            <label for="form_field">Имя пользователя</label>
			            <input type="text" class="form_field" placeholder="Имя пользователя" name="username">
			         </div>
			         <div class="wrapper_field wrapper_field-input">
			            <label for="form_field">Сообщение</label>
			            <textarea name="message" class="user_message" placeholder="Сообщение"></textarea>
			         </div>
			         <input type="hidden" name="id_article" value="<?php echo $page['id_article']; ?>">
			         <div class="wrapper_field wrapper_field-input">
			            <input type="submit" value="Отправить" name="send_comment" class="btn btn-danger">
			         </div>
				</form>

				<div class="show_comments">
					<?php 
						if($comments){
							foreach($comments as $comment){
								echo '<div class="comment">';
								echo '<div class="username">'.'<span class="points">•</span>'.$comment['username'].' <span class="date">'.date("d/m/Y", strtotime($comment['date_val'])).'</span></div>';
								echo '<div class="message">'.$comment['message'].'</div>';
								echo '</div>';
							}
						}else{
							echo '<div>Комментарии отсутствуют</div>';
						}
						
					?>
				</div>
			</div>
		</div>
	</div>
	<!-- Sidebar -->
	<div class="sidebar sidebar_article">
		
		<!-- Search -->
		<div class="sidebar_search">
			<div class="sidebar_title">Поиск</div>
			<form action="/search/" class="sidebar_search_form" method="POST">
				<input type="text" class="sidebar_search_input" placeholder="Что ищем?" required="required" name="query">
				<input type="submit" class="sidebar_search_button" name="search" value="Поиск">
			</form>
		</div>

		<?php if($categories):?>
			<div class="categories">
				<div class="sidebar_title">Категории</div>
				<div class="sidebar_list">
					<ul>
						<?php foreach($categories as $category):?>
							<li><a href="/search/<?php echo $category['title']; ?>"><?php echo $category['title']; ?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
		<?php endif;?>

		<?php if($left_menu):?>
		<div class="categories">
			<div class="sidebar_title">Страницы</div>
			<div class="sidebar_list">
				<ul>
					<?php foreach($left_menu as $item):?>
						<li><a href="/<?php echo $item['full_cache_url'];?>"><?php echo $item['title_in_menu'];?></a></li>
					<?php endforeach;?>
				</ul>
			</div>
		</div>
		<?php endif; ?>

	</div>
</div>
