<div class="login_page">
	<?php 
		if($error){
			echo '<div class="error">Неверный логин или пароль</div>';
		}
	?>
    <form method="post">
        <input type="text" class="form-control" placeholder="Username" name="username" autofocus>
        <input type="password" class="form-control" placeholder="Password" name="password">
        <input type="submit" class="btn btn-primary btn-block" name="autorize" value="Sign In">
    </form>
</div>