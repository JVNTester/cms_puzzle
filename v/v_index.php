<?php echo $gallery; ?>

<section class="achievements">
    <div class="achievement achievement--dark">
        <div class="achievement_img"><img src="/<?php echo ALL_IMG_DIR;?>icon_1.svg" alt=""></div>
        <div class="achievement_title">aerobics program</div>
        <div class="achievement_desc">Aenean auctor nisl vitae auctor faucibus. Pellentesque imperdiet auctor eros, sit amet ornare mauris malesuada in. Duis rutrum nisi tempus finibus luctus.</div>
    </div>
    <div class="achievement achievement--blue">
        <div class="achievement_img"><img src="/<?php echo ALL_IMG_DIR;?>icon_2.svg" alt=""></div>
        <div class="achievement_title">train hard</div>
        <div class="achievement_desc">Sed porta vel lacus quis lacinia. Vestibulum nec justo lectus. In hac habitasse platea dictumst. Proin vulputate rhoncus nibh eu vehicula.Sed porta vel lacus quis lacinia.</div>
    </div>
    <div class="achievement achievement--red">
        <div class="achievement_img"><img src="/<?php echo ALL_IMG_DIR;?>icon_3.svg" alt=""></div>
        <div class="achievement_title">basic program</div>
        <div class="achievement_desc">Aenean auctor nisl vitae auctor faucibus. Pellentesque imperdiet auctor eros, sit amet ornare mauris malesuada in. Duis rutrum nisi tempus finibus luctus.</div>
    </div>
</section>
<section class="about">
	<div class="about_left">
		<div class="about_left__subtitle">aerobics program</div>
		<div class="about_left__title">about sportify</div>
		<div class="about_left__desc">Aenean auctor nisl vitae auctor faucibus. Pellentesque imperdiet auctor eros, sit amet ornare mauris malesuada in. Duis rutrum nisi tempus finibus luctus. Sed porta vel lacus quis lacinia. Vestibulum nec justo lectus. In hac habitasse platea dictumst. Proin vulputate rhoncus nibh eu vehicula. Donec vitae laoreet quam, ac feugiat nibh. Aenean auctor nisl vitae auctor faucibus. Pellentesque imperdiet auctor eros, sit amet ornare mauris malesuada in. Duis rutrum nisi tempus finibus luctus.</div>
	</div>
	<div class="about_right">
		<div class="about_img">
			<img src="/<?php echo ALL_IMG_DIR;?>about.png" alt="">
		</div>
	</div>
</section>
<section class="quote">
	<div class="quote__text">“Training gives us an outlet for suppressed energies created by stress and thus tones the spirit just as exercise conditions the body.”</div>
	<div class="quote__author">– Arnold Schwarzenegger</div>
</section>