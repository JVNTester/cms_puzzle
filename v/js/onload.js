$(document).ready(function(){

	$('.main_nav > li').click(function(){
		var child = $(this).find('ul');
		if(child){
			$(this).children('ul').slideToggle();
			var angle = $(this).find('a > i');
			if(angle[1].classList.contains('fa-angle-down')){
				angle[1].classList.remove('fa-angle-down');
				angle[1].classList.add('fa-angle-left');
			}else{
				angle[1].classList.remove('fa-angle-left');
				angle[1].classList.add('fa-angle-down');
			}
		}
	});
});