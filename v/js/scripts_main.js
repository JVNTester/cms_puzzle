$(document).ready(function(){
	$('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    dots: false,
	    items:1,
	    autoplay:true,
	    autoplayTimeout:4000,
	    smartSpeed: 750,
	    // autoplayHoverPause:true,
	    responsive:{
	        0:{
	            // items:1
	        },
	        600:{
	            // items:3
	        },
	        1000:{
	            // items:5
	        }
	    }
	})
});