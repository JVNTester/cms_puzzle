var arr_report = [['Месяц', window.column_report]];
for(var one in window.dataReport){
  arr_report.push([one, window.dataReport[one]])
}
google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable(
    arr_report
  );

  var options = {
    chart: {
      title: window.textReport,
    },
    bars: 'horizontal' // Required for Material Bar Charts.
  };

  var chart = new google.charts.Bar(document.getElementById('barchart_material'));

  chart.draw(data, google.charts.Bar.convertOptions(options));
}
