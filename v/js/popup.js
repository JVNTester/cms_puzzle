function Popup(){
	var Popup = this;
	this.popup = document.createElement('div');
	this.popup.classList.add('popup');
	this.overlay = document.createElement('div');
	this.overlay.classList.add('overlay');
	document.body.appendChild(this.popup);
	document.body.appendChild(this.overlay);

    this.open = function(content){
    	Popup.popup.innerHTML = content;
        var overlay = document.querySelector('.overlay');
        var popup = document.querySelector('.popup');
        overlay.classList.add('show_overlay');
        popup.classList.add('show_popup');
    }
    
    this.close = function(){
        Popup.popup.classList.remove('show_popup');
        Popup.overlay.classList.remove('show_overlay');

    	Popup.popup.innerHTML = '';
    	Popup.overlay.innerHTML = '';
    }
    this.overlay.onclick = function(){
    	Popup.close();
    }
}