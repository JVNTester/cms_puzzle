<!DOCTYPE html>
<html>
<head>
    <title>Админка</title>
    <meta content="text/html; charset=Windows-1251" http-equiv="content-type
    ">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <?php
    foreach($css_plugins as $plugin){
        echo "<link rel=\"stylesheet\" href=\"/" .PLUGIN_DIR. "/".$plugin.".css\">\r\n";
    }
    ?>
    <?php
        foreach($styles as $style){
            echo "<link rel=\"stylesheet\" href=\"/" .CSS_DIR. "/".$style.".css\">\r\n";
        }
    ?>
</head>
<body>
<div class="wrapper">
    <aside class="sidebar">
        <div class="logo_link_wrapper">
            <a href="/pages" class="logo_link">
                <div class="logo">
                    <img src="/v/img/AdminLTELogo.png" alt="AdminLTE Logo">
                </div>
                <span>Админка</span>
            </a>
        </div>

        <!-- Sidebar -->
        <div class="sidebar_inner">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="image">
                    <img src="/v/img/avatar5.png" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="user">
                    <?php echo $user['username']; ?>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav>
                <ul class="main_nav">
                    <?php if(M_Helpers::can_look('EDIT_PAGES')):?>
                    <li>
                        <a href="#">
                            <i class="nav-icon fas fa-chart-pie"></i>
                            Страницы
                            <i class="right fas fa-angle-left"></i>
                        </a>
                        <ul>
                            <li class="nav-item">
                                <a href="/pages/">
                                    <i class="far fa-circle nav-icon"></i>
                                    Все
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/pages/add">
                                    <i class="far fa-circle nav-icon"></i>
                                    Добавить новую
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php endif;?>
                    <?php if(M_Helpers::can_look('EDIT_CONTACT_INF')):?>
                    <li>
                        <a href="/contact_inf/">
                            <i class="nav-icon fas fa-chart-pie"></i>
                            Контактная информация
                        </a>
                    </li>
                    <?php endif;?>
                    <?php if(M_Helpers::can_look('EDIT_ARTICLES')):?>
                    <li>
                        <a href="#">
                            <i class="nav-icon fas fa-copy"></i>
                            Статьи
                            <i class="fas fa-angle-left right"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="/adarticle/">
                                    <i class="far fa-circle nav-icon"></i>
                                    Все
                                </a>
                            </li>
                            <li>
                                <a href="/adarticle/add">
                                    <i class="far fa-circle nav-icon"></i>
                                    Добавить новую
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php if(M_Helpers::can_look('ALL')):?>
                        <li>
                            <a href="#">
                                <i class="nav-icon fas fa-copy"></i>
                                Меню
                                <i class="fas fa-angle-left right"></i>
                            </a>
                            <ul>
                                <li>
                                    <a href="/menu/">
                                        <i class="far fa-circle nav-icon"></i>
                                        Все
                                    </a>
                                </li>
                                <li>
                                    <a href="/menu/add">
                                        <i class="far fa-circle nav-icon"></i>
                                        Добавить новое
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif;?>
                        <?php if(M_Helpers::can_look('ALL') || M_Helpers::can_look('ADD_CATEGORY')):?>
                        <li>
                            <a href="#">
                                <i class="nav-icon fas fa-copy"></i>
                                Категории
                                <i class="fas fa-angle-left right"></i>
                            </a>
                            <ul>
                                <li>
                                    <a href="/categoryes_admin/">
                                        <i class="far fa-circle nav-icon"></i>
                                        Все
                                    </a>
                                </li>
                                <li>
                                    <a href="/categoryes_admin/add">
                                        <i class="far fa-circle nav-icon"></i>
                                        Добавить новую
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif;?>
                        <?php if(M_Helpers::can_look('ALL')): ?>
                        <li>
                            <a href="#">
                                <i class="nav-icon fas fa-copy"></i>
                                Шаблоны
                                <i class="fas fa-angle-left right"></i>
                            </a>
                            <ul>
                                <li>
                                    <a href="/templates/">
                                        <i class="far fa-circle nav-icon"></i>
                                        Все
                                    </a>
                                </li>
                                <li>
                                    <a href="/templates/add">
                                        <i class="far fa-circle nav-icon"></i>
                                        Добавить новый
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif;?>
                        <?php if(M_Helpers::can_look('ALL') || M_Helpers::can_look('ADD_GALLERY')):?>
                        <li>
                            <a href="#">
                                <i class="nav-icon fas fa-copy"></i>
                                Галереи
                                <i class="fas fa-angle-left right"></i>
                            </a>
                            <ul>
                                <li>
                                    <a href="/gallery/">
                                        <i class="far fa-circle nav-icon"></i>
                                        Все
                                    </a>
                                </li>
                                <li>
                                    <a href="/gallery/add">
                                        <i class="far fa-circle nav-icon"></i>
                                        Добавить новую
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif;?>
                        <?php if(M_Helpers::can_look('ALL') || M_Helpers::can_look('EDIT_USERS')):?>
                        <li>
                            <a href="#">
                                <i class="nav-icon fas fa-copy"></i>
                                Пользователи
                                <i class="fas fa-angle-left right"></i>
                            </a>
                            <ul>
                                <li>
                                    <a href="/users/">
                                        <i class="far fa-circle nav-icon"></i>
                                        Все
                                    </a>
                                </li>
                                <li>
                                    <a href="/users/add">
                                        <i class="far fa-circle nav-icon"></i>
                                        Добавить нового
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif;?>
                        <?php if(M_Helpers::can_look('ALL') || M_Helpers::can_look('WATCH_REPORTS')):?>
                        <li>
                            <a href="/reports">
                                <i class="nav-icon fas fa-copy"></i>
                                Отчеты
                            </a>
                        </li>
                        <?php endif; ?>
                    <li>
                        <a href="/login">
                            <i class="nav-icon fas fa-copy"></i>
                            Выход
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
<div class="content-wrapper">
    <div class="content">
        <?php echo $content; ?>
    </div>

    <div class="footer">
        &copy;Copyright 2020 Project: CMS-Puzzle 
    </div>
</div>

<?php
    foreach($js_plugins as $script){
        echo "<script src=\"/".PLUGIN_DIR."/".$script.".js\"></script>";
    }
    foreach($js_additional as $script){
        echo '<script src="'.$script.'"></script>';
    }
?>
<?php
    foreach($scripts as $script){
        echo "<script src=\"/".JS_DIR."/".$script.".js\"></script>";
    }
?>
</body>
</html>
