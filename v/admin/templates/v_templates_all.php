<div class="h2"><?php echo $title; ?></div>
<a href="/templates/add" class="btn btn-block btn-default create_item">Создать новый шаблон</a>
<div class="left">
	<ul class="map">
	    <?php 
	    	foreach($templates as $template){
	    		echo '<li><a href="/templates/edit/'.$template['id_template'].'">'.$template['name_template'].'</a></li>';
	    	}
	    ?>
    </ul>
</div>