<?php
    if($error){
        echo '<div class="error">Заполните все поля!</div>';
    }
?>
<div id="usersettings">
    <div class="h2"><?php echo $title; ?></div>
    <form method="post">
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Полное название</label>
            <input type="text" class="form_field" placeholder="Полное название" name="full_template_name" value="<?php echo $fields['full_template_name']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Название шаблона</label>
            <input type="text" class="form_field" placeholder="Заголовок в меню" name="name_template" value="<?php echo $fields['name_template']; ?>">
        </div>
        <input type="submit" value="Сохранить шаблон" class="btn btn-success" name="save_data">
        <input type="submit" value="Удалить шаблон" class="btn btn-danger" name="remove_data">
    </form>
</div>