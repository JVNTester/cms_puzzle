<div class="h2"><?php echo $title . ' ' . $fields['username']; ?></div>

<?php if($error): ?>
	<p class="error">Ошибка</p>
<?php endif;?>
<form method="post">
	<input name="id_user" type="hidden" value="<?php echo $fields['id_user']; ?>"/>
	
	<div class="wrapper_field wrapper_field-input">
            <label for="form_field">Имя пользователя</label>
            <input name="username" id="login" type="text" class="input-xlarge" value="<?php echo $fields['username']; ?>"/>
    </div>
	<?php if(M_Helpers::can_look('DELETE_USERS')): ?>
    <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Пароль</label>
           <input name="password" id="password" type="password" class="input-xlarge">
           (leave blank if you do not want to make changes)
    </div>
	<?php endif;?>
    <div class="wrapper_field wrapper_field-select">
            <label for="form_field">Роль</label>
          	<select name="id_role">
				<? foreach($roles as $role): ?>
					<option value="<?php echo $role['id_role'] ?>" <?php if($role['id_role'] == $fields['id_role']) echo 'selected';?>>
						<?php echo $role['description']; ?>
					</option>
				<? endforeach ?>
			</select>
    </div>
	<input type="submit" name="update" class="btn btn-success" value="Save"/>
	<?php if($fields['id_user'] != 1 && M_Helpers::can_look('DELETE_USERS')): ?>
		<input type="submit" name="delete" class="btn btn-danger" value="Delete"/>
	<?php endif; ?>
	<br/>
</form>

