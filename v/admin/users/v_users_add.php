<div class="h2"><?php echo $title; ?></div>

<?php if($error): ?>
	<p class="error">Ошибка при добавлении!</p>
<?php endif;?>
<form method="post">
	<div class="wrapper_field wrapper_field-input">
            <label for="form_field">Имя пользователя</label>
            <input name="username" id="login" type="text" class="input-xlarge" value="<?php echo $fields['username']; ?>"/>
    </div>
    <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Пароль</label>
           <input name="password" id="password" type="password" class="input-xlarge">
    </div>
    <div class="wrapper_field wrapper_field-select">
            <label for="form_field">Роль</label>
          	<select name="id_role">
				<? foreach($roles as $role): ?>
					<option value="<?php echo $role['id_role'] ?>" <?php if($role['id_role'] == $fields['id_role']) echo 'selected';?>>
						<?php echo $role['description']; ?>
					</option>
				<? endforeach ?>
			</select>
    </div>
	
	<input type="submit" class="btn btn-success" value="Create">
</form>
