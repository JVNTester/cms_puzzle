<?php
function print_tree($map, $pages, $shift = 0){
    if(!empty($map)) {
        foreach($map as $section) {
            echo '<option value="'.$section['id_page'].'" ';
                if(in_array($section['id_page'], $pages)){
                    echo 'selected';
                }
            echo '>';
            for($i = 0; $i < $shift; $i++){
                echo '&nbsp;';
            }
            echo $section['title'] . '</option>';
            print_tree($section['children'], $pages,$shift + 5);

        }
    }
}
?>
<? if($error):?>
    <span style="color: red;">Заполните все поля</span>
<?endif?>
<div id="usersettings">
    <div class="h2"><?php echo $title; ?></div>
    <form method="post">
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Название меню</label>
            <input type="text" class="form_field" placeholder="Название меню" name="title" value="<?php echo $fields['title']; ?>" autofocus>
        </div>

        <div class="wrapper_field wrapper_field-select">
            <label for="form_field">Добавить страницы</label>
             <select name="pages[]" id="pages" multiple class="menu">
                <?php print_tree($map, $fields['pages']) ?>
            </select>
        </div>
        <input type="submit" value="Сохранить" class="btn btn-success">
    </form>
</div>