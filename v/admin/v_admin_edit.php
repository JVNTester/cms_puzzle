<?php
function print_tree($map, $id_parent, $shift = 0){
    if(!empty($map)) {
        foreach($map as $section) {
            echo '<option value="'.$section['id_page'].'" ';
            if($id_parent == $section['id_page']){
                echo 'selected';
            }
            echo '>';
            for($i = 0; $i < $shift; $i++){
                echo '&nbsp;';
            }
            echo $section['title'] . '</option>';
            print_tree($section['children'], $id_parent, $shift + 5);

        }
    }
}
?>
<?php
if($error){
    echo '<div class="error">Заполните все поля!</div>';
}
?>
<div id="usersettings">
    <div class="h2"><?php echo $title; ?></div>
    <form method="POST" class="form_sorting">
        <div class="wrapper_field wrapper_field-select">
            <label for="form_field">Раздел</label>
            <select name="id_parent">
                <option value="0">Без раздела</option>
                <?php print_tree($map, $fields['id_parent']); ?>
            </select>
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Адрес страницы</label>
            <input type="text" class="form_field" placeholder="URL" name="url" value="<?php echo $fields['url']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Заголовок в меню</label>
            <input type="text" class="form_field" placeholder="Заголовок в меню" name="title_in_menu" value="<?php echo $fields['title_in_menu']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Заголовок страницы</label>
            <input type="text" class="form_field" placeholder="Заголовок в меню" name="title" value="<?php echo $fields['title']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Ключевые слова</label>
            <input type="text" class="form_field" placeholder="Ключевые слова" name="keywords" value="<?php echo $fields['keywords']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Описание страницы</label>
            <input type="text" class="form_field" placeholder="Описание страницы" name="description" value="<?php echo $fields['description']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Выбор шаблона</label>
            <select name="id_template">
                <?php
                    foreach($templates as $template){
                        if($fields['id_template'] == $template['id_template']){
                            echo '<option value="'.$template['id_template'].'" selected>'.$template['name_template'].'</option>';
                        }else{
                            echo '<option value="'.$template['id_template'].'">'.$template['name_template'].'</option>';
                        }
                    }
                ?>
            </select>
        </div>
        <div class="wrapper_field wrapper_field-select">
            <label for="form_field">Выбор Меню</label>
               <select name="id_menu">
                        <option value="0">Дочерние разделы</option>
                        <?php
                        foreach($menus as $menu){
                            if($fields['id_menu'] == $menu['id_menu']){
                                echo '<option value="'.$menu['id_menu'].'" selected>'.$menu['title'].'</option>';
                            }else{
                                echo '<option value="'.$menu['id_menu'].'">'.$menu['title'].'</option>';
                            }
                        }
                        ?>
               </select>
        </div>
        <div class="wrapper_field wrapper_field-checkbox">
            <label for="form_field">Страница блога</label>
            <input type="checkbox" name="is_blog" <?if($fields['is_blog']) echo 'checked';?>>
        </div>
        <textarea id="replace" rows="8" name="content"><?php echo $fields['content']; ?></textarea><br>
        <div class="wrapper_field wrapper_field-checkbox">
            <label for="form_field">Отображение страницы</label>
            <input type="checkbox" name="is_show" <?php if($fields['is_show']) echo 'checked';?>>
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Сортировка дочерних страниц</label>
            <ul class="sorting">
                <?php
                    foreach($children as $child){
                        echo '<li data-id="'.$child['id_page'].'">'.$child['title'].'</li>';
                    }
                ?>
            </ul>
        </div>
        <input type="hidden" name="pages" value="">
        <input type="button" value="Сохранить изменения" class="btn btn-success" name="go">
        <input type="submit" value="Удалить" class="btn btn-danger" name="remove">
    </form>
</div>