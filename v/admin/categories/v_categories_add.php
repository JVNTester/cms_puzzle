<?php
    if($error){
        echo '<div class="error">Заполните все поля!</div>';
    }
?>
<div id="usersettings">
    <div class="h2"><?php echo $title; ?></div>
    <form method="post">
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Название категории</label>
            <input type="text" class="form_field" placeholder="Полное название" name="title" value="<?php echo $fields['full_template_name']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-select">
            <label for="form_field">Выбор страниц</label>
            <select name="id_pages[]" multiple>
                <?php 
                    foreach($articles as $article){
                        echo '<option value="'.$article['id_article'].'">'.$article['title'].'</option>';
                    }
                ?>
            </select>
        </div>
        <input type="submit" value="Создать шаблон" class="btn btn-success" name="save_data">
    </form>
</div>