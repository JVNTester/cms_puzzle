<?php
    if($error){
        echo '<div class="error">Заполните все поля!</div>';
    }
    $used = [];
    for($i = 0; $i < count($fields[1]); $i++){
        $used[] = $fields[1][$i];
    }
?>
<div id="usersettings">
    <div class="h2"><?php echo $title; ?></div>
    <form method="post">
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Название категории</label>
            <input type="text" class="form_field" placeholder="Полное название" name="title" value="<?php echo $fields[0][0]['title']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-select">
            <label for="form_field">Выбор страниц</label>
            <select name="id_pages[]" multiple>
                <?php 
                    foreach($articles as $article){
                        if(in_array((int)$article['id_article'], $used)){
                            echo '<option selected value="'.$article['id_article'].'">'.$article['title'].'</option>';
                        }else{
                            echo '<option value="'.$article['id_article'].'">'.$article['title'].'</option>';
                        }
                        
                    }
                ?>
            </select>
        </div>
        <input type="submit" value="Сохранить категорию" class="btn btn-success" name="save_data">
        <input type="submit" value="Удалить категорию" class="btn btn-danger" name="remove">
    </form>
</div>