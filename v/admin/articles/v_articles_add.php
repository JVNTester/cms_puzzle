<?php
    if($error){
        echo '<div class="error">Заполните все поля!</div>';
    }
?>
<div id="usersettings">
    <div class="h2"><?php echo $title; ?></div>
    <form method="post">
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Адрес статьи</label>
            <input type="text" class="form_field" placeholder="URL" name="url" value="<?php echo $fields['url']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Заголовок статьи</label>
            <input type="text" class="form_field" placeholder="Заголовок статьи" name="title" value="<?php echo $fields['title']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Автор статьи</label>
            <input type="text" class="form_field" placeholder="Автор статьи" name="autor" value="<?php echo $fields['autor']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Описание статьи(короткое)</label>
            <input type="text" class="form_field" placeholder="Описание статьи" name="description" value="<?php echo $fields['description']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Изображение статьи</label>
            <input type="text" class="form_field" placeholder="Изображение" name="image" value="<?php echo $fields['image']; ?>">
        </div>
        <div class="wrapper_field wrapper_field-checkbox">
            <label for="form_field">Отображение категорий</label>
            <input type="checkbox" name="show_category" <?if(isset($fields['is_show'])) echo 'checked';?>>
        </div>
        <div class="wrapper_field wrapper_field-select">
            <label for="form_field">Выбор Меню</label>
                <select name="id_menu">
                    <option value="0">Отсутствует</option>
                    <?php
                    foreach($menus as $menu){
                        if($fields['id_menu'] == $menu['id_menu']){
                            echo '<option value="'.$menu['id_menu'].'" selected>'.$menu['title'].'</option>';
                        }else{
                            echo '<option value="'.$menu['id_menu'].'">'.$menu['title'].'</option>';
                        }
                    }
                    ?>
                </select>
        </div>
        <div class="wrapper_field wrapper_field-input">
            <label for="form_field">Выбор шаблона</label>
            <select name="id_template">
                <?php
                    foreach($templates as $template){
                        if($fields['id_template'] == $template['id_template']){
                            echo '<option value="'.$template['id_template'].'" selected>'.$template['name_template'].'</option>';
                        }else{
                            echo '<option value="'.$template['id_template'].'">'.$template['name_template'].'</option>';
                        }
                    }
                ?>
            </select>
        </div>
        <textarea id="replace" rows="8" name="content"><?php echo $fields['content']; ?></textarea><br>
        <div class="wrapper_field wrapper_field-checkbox">
            <label for="form_field">Отображение страницы</label>
            <input type="checkbox" name="is_show" <?if(isset($fields['is_show'])) echo 'checked';?>>
        </div>
        <input type="submit" value="Создать страницу" class="btn btn-success" name="save_data">
    </form>
</div>