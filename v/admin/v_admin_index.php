<?php
    function print_map($map){
        if(!empty($map)){
            echo '<ul class="map">';
                foreach($map as $page){
                    echo "<li><a href=\"/pages/edit/" . $page['id_page'] . "\">" . $page['title_in_menu'] . "</a>";

                    print_map($page['children']);
                    echo "</li>";
                }
            echo '</ul>';
        }
    }
?>
<div class="h2"><?php echo $title; ?></div>
<a href="/pages/add" class="btn btn-block btn-default create_item">Создать новую страницу</a>
<div class="left">
    <?php print_map($map); ?>
</div>