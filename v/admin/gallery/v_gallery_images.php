<?php
    if($error){
        echo '<div class="error">Произошла ошибка!</div>';
    }
?>
<div id="usersettings">
    <div class="h2"><?php echo $title; ?></div>
    <a href="/gallery/upload/<?php echo $gallery['id_gallery']; ?>" class="btn btn-block btn-default create_item">Upload images</a><br><br>
    <div>
        <input type="button" id="btn_save" value="Save sorting" class="btn btn-danger">
        <span id="msg_save">Saved!</span>
    </div>
    <?php if(count($images) > 0){
        echo '<ul id="gallery_sortable" class="noicons noshifts">';
                foreach($images as $img){
                    echo '<li class="delimg" data-id="' . $img['id_image'] . '">';
                        echo '<form method="post">';
                            echo '<input type="submit" class="delete" value="">';
                            echo '<input type="hidden" name="id_gallery" value="' . $img['id_gallery'] . ' ">';
                            echo '<input type="hidden" name="id_image" class="delete" value="' . $img['id_image'] . '">';
                        echo '</form>';
                        //echo '<a href="/gallery/names/' . $img['id_image'] . '">Edit image</a>';
                        echo '<img class="im" src="/' . IMG_SMALL_DIR . $img['path'] . '">';
                    echo '</li>';
                }

        echo '</ul>';
    }else{
        echo '<p>There are no images</p>';
    }
    ?>
</div>
<div class="clear"></div>
<form method="post">
    <input type="hidden" name="id_gallery" value="<?php echo $gallery['id_gallery']; ?>">
    <input type="submit" name="delete_gallery" value="Удалить галерею" class="btn btn-danger">
</form>