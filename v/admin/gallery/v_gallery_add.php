<?php
    if($error){
        echo '<p class="error">Заполни все поля!</p>';
    }
?>
<div id="usersettings">
    <form method="post">
        <div class="h2"><?php echo $title; ?></div>
        <div class="control-group">
            <label class="control-label" for="name">Имя в системе</label>
            <div class="controls">
                <div class="input-prepend">
                    <input type="text" name="name" id="name" value="<?php echo $fields['name']; ?>">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="title">Название</label>
            <div class="controls">
                <div class="input-prepend">
                    <input type="text" name="title" id="title" value="<?php echo $fields['title']; ?>">
                </div>
            </div>
        </div>
        <br><br>
        <input type="submit" value="Создать галерею" class="btn btn-success">
    </form>
</div>