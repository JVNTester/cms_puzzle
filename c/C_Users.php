<?php

class C_Users extends C_Admin_Base{
    protected $title;
    protected $content;
    protected $error;
    protected $fields;
    protected $M_Users;

    public function __construct(){
        $this->M_Users = new M_Users();
        $this->error = false;
        $this->fields = [];
        if(!M_Helpers::can_look('EDIT_USERS')){
            $this->redirect('login');
        }
    }

    public function action_index(){
        $this->title = "Все пользователи";
        $users = $this->M_Users->getAllUsers();

        $this->content = $this->Template('v/admin/users/v_users_all.php',[
            'title' => $this->title, 'users' => $users
        ]);
    }

    public function action_add(){
        $this->title = "Добавление пользователей";
        $roles = $this->M_Users->getRoles();

        if($_POST){
            if($this->M_Users->addUser($_POST)){
                $this->redirect("users/");
            }else{
                $this->fields = $_POST;
                $this->error = true;
            }
        }

        $this->content = $this->Template('v/admin/users/v_users_add.php', [
            'title' => $this->title, 'roles' => $roles, 'error' => $this->error, 'fields' => $this->fields
        ]);
    }

    public function action_edit(){
        $this->title = "Редактирование пользователя";
        $id_user = (int)$this->params[2];
        $fields = $this->M_Users->getUserDataById($id_user)[0];
        $roles = $this->M_Users->getRoles();
//        $this->dump($roles);
        if($_POST){
            if($_POST['update']){
                if($this->M_Users->updateUser($id_user, $_POST)){
                    $this->redirect("users/");
                }else{
                    $this->fields = $_POST;
                    $this->error = true;
                }
            }else if($_POST['delete']){
                if($this->M_Users->deleteUser($id_user)){
                    $this->redirect("users/");
                }else{
                    $this->fields = $_POST;
                    $this->error = true;
                }
            }else{
                $this->fields = $_POST;
                $this->error = true;
            }
        }

        $this->content = $this->Template('v/admin/users/v_users_edit.php', [
            'title' => $this->title, 'fields' => $fields, 'roles' => $roles
        ]);
    }

}