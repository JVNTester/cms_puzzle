<?php

class C_Gallery extends C_Admin_Base{
    protected $title;
    protected $content;
    protected $M_Gallery;
    private $fields;
    private $error;

    public function __construct(){
        $this->error = false;
        if(!M_Helpers::can_look('ADD_GALLERY')){
            $this->redirect('login');
        }
    }

    public function action_index(){
        $this->M_Gallery = new M_Gallery();
        parent::onInput();
        $this->title = "Все Альбомы";
        $gallery = $this->M_Gallery->getAllGallery();

        $this->content = $this->Template('v/admin/gallery/v_gallery_all.php', [
            'gallery' => $gallery, 'title' => $this->title
        ]);
    }

    public function action_add(){
        parent::onInput();
        $this->M_Gallery = new M_Gallery();
        $this->title = "Добавление галереи";
        $this->fields = [];

        if($_POST){
            if($id_gallery = $this->M_Gallery->addGallery($_POST)){
                $this->redirect('gallery/images/' . $id_gallery);
            }else{
                $this->error = true;
                $this->fields = $_POST;
            }
        }

        $this->content = $this->Template('v/admin/gallery/v_gallery_add.php',[
            'fields' => $this->fields, 'error' => $this->error, 'title' => $this->title
        ]);
    }

    public function action_upload(){
        parent::onInput();
        $this->title = "Добавление изображений";
        $this->scripts[] = "imageuploader";
        $this->M_Gallery = new M_Gallery();

        $gallery = $this->M_Gallery->getGallery($this->params[2]);


        $this->content = $this->Template('v/admin/gallery/v_gallery_upload.php', [
            'title' => $this->title, 'gallery' => $gallery
        ]);
    }

    public function action_images(){
        parent::onInput();
        $this->title = "Изображения в галерее";
        $this->scripts[] = "jquery-ui-1.10.1.custom.min";
        $this->scripts[] = "image_sort";
        $this->M_Gallery = new M_Gallery();

        $gallery = $this->M_Gallery->getGallery($this->params[2]);
        $images = $this->M_Gallery->getImages($gallery['id_gallery']);

        if($_POST['delete_gallery']){
            if($this->M_Gallery->delete_gallery($this->params[2])){
                $this->redirect('gallery');
            }else{
                $this->error = true;
            }
        }
        if($_POST){
            $this->M_Gallery->delete_image($_POST['id_gallery'], $_POST['id_image']);
            $this->redirect("gallery/images/" . $_POST['id_gallery']);
        }
  

        $this->content = $this->Template('v/admin/gallery/v_gallery_images.php', [
            'title' => $this->title, 'gallery' => $gallery, 'images' => $images
        ]);
    }



}