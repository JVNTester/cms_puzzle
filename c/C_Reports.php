<?php

	class C_Reports extends C_Admin_Base{
		protected $title;
		protected $content;
        private $M_Report;

		function __construct(){
            $this->M_Report = new M_Report();
            if(!M_Helpers::can_look('WATCH_REPORTS')){
                $this->redirect('login');
            }
		}

		protected function action_index(){
            parent::onInput();
            $this->title = 'Отчеты';

            // graph count watch articles
            // var_dump($M_Report->get_count_watch_articles());

            // graph count add articles
            // echo '<pre>';
            // var_dump($M_Report->get_count_add_articles());
            // echo '</pre>';

            $this->content = $this->Template("v/admin/reports/v_reports_all.php", [
                'title' => $this->title
            ]);
        }

        public function action_count_watch(){
            parent::onInput();
            $this->title = 'Статистика просмотров статей за год';
            $this->js_additional[] = 'https://www.gstatic.com/charts/loader.js';
            $this->scripts[] = 'reports/count_online';
            $this->M_Report->check_online();
            $report = $this->M_Report->get_count_watch_articles();
            $column_report = 'Количество просмотров статей';
            $id_google_block = "barchart_material";

            $this->content = $this->Template("v/admin/reports/v_report.php", [
                'title' => $this->title, 'data' => $report, 'column_report' => $column_report, 'id_google_block' => $id_google_block
            ]);
        }

        public function action_count_add(){
            parent::onInput();
            $this->title = 'Статистика добавления статей за год';
            $this->js_additional[] = 'https://www.gstatic.com/charts/loader.js';
            $this->scripts[] = 'reports/reports';
            $this->M_Report->check_online();
            $report = $this->M_Report->get_count_add_articles();
            $column_report = 'Количество добавленных статей';
            $id_google_block = 'columnchart_material';

            $this->content = $this->Template("v/admin/reports/v_report.php", [
                'title' => $this->title, 'data' => $report, 'column_report' => $column_report, 'id_google_block' => $id_google_block
            ]);
        }

        public function action_count_online(){
            parent::onInput();
            $this->title = 'Статистика просмотров статей за год';
            $this->js_additional[] = 'https://www.gstatic.com/charts/loader.js';
            $this->scripts[] = 'reports/reports';
            $this->M_Report->check_online();
            $report = $this->M_Report->get_online_in_year();
            $column_report = 'Количество пользователей';
            $id_google_block = 'columnchart_material';

            $this->content = $this->Template("v/admin/reports/v_report.php", [
                'title' => $this->title, 'data' => $report, 'column_report' => $column_report, 'id_google_block' => $id_google_block
            ]);
        }
	}