<?php

	class C_ContactInf extends C_Admin_Base{
		protected $title;
		protected $content;
		protected $needLogin;
		protected $user;
		protected $M_Model;
		private $error;

		function __construct(){
		    $this->M_Model = new M_Model();
            $this->error = false;
            if(!M_Helpers::can_look('EDIT_CONTACT_INF')){
                $this->redirect('login');
            }
		}

		public function action_index(){
            parent::onInput();
            $this->title = 'Контакты';
            $data = $this->M_Model->getContactInf();

            if($_POST){
            	if($this->M_Model->save_data_inf($_POST)){
            		$this->redirect('contact_inf');
            	}
            }

            $this->content = $this->Template('v/admin/v_contact_footer.php',array(
                'title'=>$this->title, 'data' => $data
            ));
        }
	}