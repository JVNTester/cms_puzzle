<?php

	class C_Articles extends C_Admin_Base{
		protected $title;
		protected $content;
		private $M_Menu;
		private $M_Pages;
		private $error;

		function __construct(){
            $this->error = false;
            $this->M_Articles = new M_Articles();
            $this->M_Pages = new M_Pages();
            $this->M_Templates = new M_Templates();
            $this->M_Menu = new M_Menu();
            if(!M_Helpers::can_look('EDIT_ARTICLES')){
                $this->redirect('login');
            }
		}

		public function action_index(){
            parent::onInput();
            $this->title = 'Статьи сайта';
            $articles = $this->M_Articles->getArticles(true);

            $this->content = $this->Template('v/admin/articles/v_articles_all.php',array(
                'title'=>$this->title, 'articles' => $articles
            ));
        }

        public function action_add(){
            parent::onInput();
            $this->title = 'Добавление статьи';
            $this->content = "content";
            $this->scripts[] = 'ckeditor/ckeditor';
            $this->scripts[] = 'ck_init';
            $fields = [];
            $templates = $this->M_Templates->getTemplates();
            $menus = $this->M_Menu->getAllMenus();

            if(isset($_POST['save_data'])){
                $add = $this->M_Articles->add_data($_POST);
                if(!$add){
                    $this->error = true;
                    $fields = $_POST;
                }else{
                    $this->redirect('adarticle');
                }
            }

            $this->content = $this->Template('v/admin/articles/v_articles_add.php',array(
                'title'=>$this->title, 'error' => $this->error, 'fields' => $fields, 'templates' => $templates, 'menus' => $menus
            ));
        }

        public function action_edit(){
            parent::onInput();
            $this->title = 'Редактирование статьи';
            $this->content = "content";
            $this->scripts[] = 'ckeditor/ckeditor';
            $this->scripts[] = 'ck_init';
            $templates = $this->M_Templates->getTemplates();
            $id_page = (int)$this->params[2];
            $fields = $this->M_Articles->getDataArticle($id_page)[0];
            $menus = $this->M_Menu->getAllMenus();
            if($_POST){
                $add = $this->M_Articles->edit_data($_POST, $id_page);
                if(!$add){
                    $this->error = true;
                    $fields = $_POST;
                }else{
                    $this->redirect('adarticle');
                }
            }

            $this->content = $this->Template('v/admin/articles/v_articles_edit.php',[
                'title'=>$this->title, 'content' => $this->content, 'error' => $this->error, 'fields' => $fields, 'templates' => $templates, 'menus' => $menus
            ]   );
        }
	}