<?php
abstract class C_Base extends Controller{
	protected $title;
	protected $type;
	protected $content;
    protected $styles;
    protected $scripts;
    protected $keywords;
    protected $description;
    protected $topMenu;
    protected $left_menu;

    protected $M_Menu;
    private $M_Model;
    private $contact_inf;

	function __construct(){
	}

	protected function onInput(){
        $this->M_Menu  = new M_Menu();
        $this->M_Model = new M_Model();

		$this->title = 'tester';
		$this->content = "content";
		$this->keywords = "";
		$this->description = "";
        $this->styles = ['font-awesome-4.7.0/css/font-awesome.min','OwlCarousel2-2.3.4/owl.carousel','OwlCarousel2-2.3.4/owl.theme.default', 'styles_main'];
        $this->scripts = ['jquery-3.3.1.min','OwlCarousel2-2.3.4/owl.carousel', 'scripts_main'];
        $this->topMenu = $this->M_Menu->getTopMenu();
        $this->left_menu = $this->M_Menu->getMenuWithPages($this->M_Menu->getFirstMenu());
        $this->contact_inf = $this->M_Model->getContactInf();

	}
	protected function onOutput(){
	    $url = implode('/', $this->params);
        if($this->left_menu){
            foreach($this->left_menu as $key => $temp){
                $this->left_menu[$key]['is_active'] = $this->M_Menu->is_active($url, $temp['full_cache_url']);
            }
        }

        foreach($this->topMenu as $key => $temp){
            $this->topMenu[$key]['is_active'] = (int)$this->M_Menu->is_active($url, $temp['full_cache_url']);
        }
        

        $page = $this->Template('v/v_main.php',array(
			'title' => $this->title,'content'=>$this->content, 'styles' => $this->styles, 'scripts' => $this->scripts,
            'keywords' => $this->keywords, 'description' => $this->description, 'topMenu' => $this->topMenu,
            'left_menu' => $this->left_menu, 'contact_inf' => $this->contact_inf
		));
		echo $page;
	}

}
