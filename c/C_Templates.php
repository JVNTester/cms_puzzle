<?php

	class C_Templates extends C_Admin_Base{
		protected $title;
		protected $content;
        protected $M_Templates;
		private $M_Pages;
		private $error;

		function __construct(){
            $this->M_Templates = new M_Templates();
            $this->error = false;
            $this->M_Pages = new M_Pages();
            if(!M_Helpers::can_look('ALL')){
                $this->redirect('login');
            }
		}

		public function action_index(){
            parent::onInput();
            $this->title = 'Шаблоны страниц сайта';
            $templates = $this->M_Templates->getTemplates();

            $this->content = $this->Template('v/admin/templates/v_templates_all.php',array(
                'title'=>$this->title, 'templates' => $templates
            ));
        }

        public function action_add(){
            parent::onInput();
            $this->title = 'Добавление шаблона';
            $this->content = "content";
            $templates = $this->M_Templates->getTemplates();

            if(isset($_POST['save_data'])){
                $add = $this->M_Templates->add_template($_POST);
                if(!$add){
                    $this->error = true;
                    $fields = $_POST;
                }else{
                    $this->redirect('templates');
                }
            }

            $this->content = $this->Template('v/admin/templates/v_templates_add.php',array(
                'title'=>$this->title, 'content' => $this->content, 'error' => $this->error, 'fields' => $fields, 'templates' => $templates
            ));
        }

        public function action_edit(){
            parent::onInput();
            $this->title = 'Редактирование шаблона';
            $this->content = "content";
            $id_template = (int)$this->params[2];
            $fields = $this->M_Templates->getTemplateId($id_template);

            if($_POST){
                if($_POST['save_data']){
                    $add = $this->M_Templates->edit_template($_POST, $id_template);
                    if(!$add){
                        $this->error = true;
                        $fields = $_POST;
                    }else{
                        $this->redirect('templates');
                    }
                }else{
                    $remove = $this->M_Templates->remove_template($id_template);
                    if(!$remove){
                        $this->error = true;
                        $fields = $_POST;
                    }else{
                        $this->redirect('templates');
                    }
                }
            }

            $this->content = $this->Template('v/admin/templates/v_templates_edit.php',[
                'title'=>$this->title, 'content' => $this->content, 'error' => $this->error, 'fields' => $fields, 'templates' => $templates
            ]   );
        }
	}