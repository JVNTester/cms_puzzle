<?php

class C_Menu extends C_Admin_Base{
    protected $title;
    protected $content;
    protected $M_Menu;
    protected $M_Pages;
    protected $error;
    protected $fields;

    public function __construct(){
        $this->M_Pages = new M_Pages();
        $this->M_Menu = new M_Menu();
        $this->error = false;
        if(!M_Helpers::can_look('ALL')){
            $this->redirect('login');
        }
    }

    public function action_index(){
        parent::onInput();
        $this->title = "Все меню";
        $menus = $this->M_Menu->getAllMenus();

        $this->content = $this->Template('v/admin/v_menu_index.php', ['title' => $this->title,'menus' => $menus]);
    }

    public function action_add(){
        parent::onInput();
        $this->title = "Добавление меню";
        $map = $this->M_Pages->make_tree();
        $this->fields = ['pages' => []];

            if($_POST){
                if($this->M_Menu->addMenu($_POST)){
                    $this->redirect("menu");
                }else{
                    $this->fields = $_POST;
                    if(!$this->fields['pages']){
                        $this->fields['pages'] = [];
                    }
                    $this->error = true;
                }
        }

        $this->content = $this->Template('v/admin/v_menu_add.php', ['map' => $map, 'error' => $this->error, 'fields' => $this->fields, 'title' => $this->title]);
    }

    public function action_edit(){
        parent::onInput();
        $this->title = "Редактирование меню";
        $map = $this->M_Pages->make_tree();
        $idMenu = (int)$this->params[2];
        $this->fields = $this->M_Menu->getMenu($idMenu);

        if($_POST){
            if($_POST['update']){
                if($this->M_Menu->editMenu($idMenu,$_POST)){
                    $this->redirect("menu");
                }else{
                    $this->fields = $_POST;
                    if(!$this->fields['pages']){
                        $this->fields['pages'] = [];
                    }
                    $this->error = true;
                }
            }
            if($_POST['delete']){
                if($this->M_Menu->deleteMenu($idMenu)){
                    $this->redirect("menu");
                }else{
                    $this->error = true;
                }
            }
        }

        $this->content = $this->Template('v/admin/v_menu_edit.php', ['map' => $map, 'error' => $this->error, 'fields' => $this->fields, 'title' => $this->title]);
    }

    public function action_sort(){
        $this->title = "Сортировка меню";
        $this->scripts[] = "jquery-ui-1.10.1.custom.min";
        $this->scripts[] = "sortable";
        $menu = $this->M_Menu->getMenuWithPages($this->params[2]);

        if($_POST){
            $this->M_Menu->sorting($this->params[2], $_POST['pages']);
            $this->redirect("menu");

        }

        $this->content = $this->Template('v/admin/v_sort.php', ['title' => $this->title,'menu' => $menu, 'id_menu' => $this->params[2]]);
    }

}