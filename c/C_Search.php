<?php

	class C_Search extends C_Base{
		protected $title;
		protected $content;
        private $M_Articles;
        private $M_Search;
        private $M_Category;

		function __construct(){
            $this->M_Category = new M_Category();
            $this->M_Search = new M_Search();
            $this->M_Articles = new M_Articles();
		}

		protected function action_index(){
            parent::onInput();
            $this->scripts[] = "jquery-ui-1.10.1.custom.min";
            $this->scripts[] = "search";
            $this->styles[] = "jquery-ui";
            $this->title = 'Поиск';
            $categories = $this->M_Category->getCategories();
            $is_search = false;
            $is_filter = false;
            $fields = [];

            if($this->params[1]){
                $filter = $this->M_Search->checkCategory($this->params[1]);
                $is_filter = true;
            }

            if($_POST['search']){
                $is_search = true;
                $search = $this->M_Articles->getArticlesByTitle($_POST['query'])[0];
            }
            if($_POST['filter']){
                $is_filter = true;
                $filter = $this->M_Search->filter($_POST);
                $fields = $_POST;
            }
            if($_POST['clearFilters']){
                $this->redirect('search');
            }

            $this->content = $this->Template("v/v_search.php", ['title' => $this->title, 'is_search' => $is_search, 'is_filter' => $is_filter, 'search' => $search, 'is_filter' => $is_filter, 'filter' => $filter, 'categories' => $categories, 'fields' => $fields]);
        }

       
	}