<?php

	class C_Page extends C_Base{
		protected $title;
		protected $content;
        private $message;
        private $categories;
        private $commets;
		private $M_Model;
		private $M_Pages;
        private $M_Page;

		function __construct(){
		    $this->M_Model = new M_Model();
		    $this->needLogin = false;
            $this->M_Pages = new M_Pages();
            $this->M_Page = new M_Page();
            $this->M_Templates = new M_Templates();
            $this->M_Category = new M_Category();
		}

		protected function action_index(){
            // graph online
            $M_Report = new M_Report();
            // $M_Report->check_online();
            // echo '<pre>';
            // var_dump($M_Report->get_online_in_year());
            // echo '</pre>';

            // graph count watch articles
            // var_dump($M_Report->get_count_watch_articles());

            // graph count add articles
            // echo '<pre>';
            // var_dump($M_Report->get_count_add_articles());
            // echo '</pre>';

            $url = implode('/', $this->params);
            if ($url == null) {
                $url = 'home';
            }
            if($this->params[0] == 'contacts'){
                $this->action_contacts($url);
                return;
            }
            $dataPage = $this->M_Pages->getByUrl($url);

            if($dataPage['is_blog']){
                $this->action_blog($url);
                return;
            }

            if($this->params[0] == 'articles'){
                $this->action_article($this->params[1]);
                return;
            }

            if (empty($dataPage)) {
                $this->action_404();
                return;
            }

            

            parent::onInput();
            $this->title = $dataPage['title'];
            $this->description = $dataPage['description'];
            $this->keywords = $dataPage['keywords'];
            $categories = $this->M_Category->getCategories();
            $template = $this->M_Templates->getTemplateId($dataPage['id_template'])['full_template_name'];

            if($dataPage['id_menu'] != 0){
                $this->left_menu = $this->M_Menu->getMenuWithPages($dataPage['id_menu']);
            }else{
                $this->left_menu = $this->M_Pages->getByParent($dataPage['id_page']);
            }

            $this->content = $this->Template("v/$template.php", ['page' => $dataPage, 'keywords' => $this->keywords, 'description' => $this->description, 'gallery' => $this->request('widgets/gallery/1'), 'message' => $this->message, 'categories' => $categories]);
        }

        public function action_contacts($url){
            $dataPage = $this->M_Pages->getByUrl($url);
            parent::onInput();
            // $this->scripts[] = "contact";
            $this->title = $dataPage['title'];
            $this->description = $dataPage['description'];
            $this->keywords = $dataPage['keywords'];
            $template = $this->M_Templates->getTemplateId($dataPage['id_template'])['full_template_name'];
            $footer_fields = $this->M_Model->getContactInf();

            if($dataPage['id_menu'] != 0){
                $this->left_menu = $this->M_Menu->getMenuWithPages($dataPage['id_menu']);
            }else{
                $this->left_menu = $this->M_Pages->getByParent($dataPage['id_page']);
            }

            if($_POST['contact_form']){
                if($this->M_Page->contact_form($_POST)){
                    $this->message = 'Заявка отправлена!';
                }else{
                    $this->message = 'Произошла ошибка!';
                }
            }

            $this->content = $this->Template("v/$template.php", ['page' => $dataPage, 'keywords' => $this->keywords, 'description' => $this->description, 'gallery' => $this->request('widgets/gallery/1'), 'message' => $this->message, 'footer_fields' => $footer_fields]);
        }

        public function action_article($url){
            $M_Articles = new M_Articles();
            $dataPage = $M_Articles->getArticleByUrl($url);
            // $M_Articles->addCount($dataPage['id_article']);

            $this->title = $dataPage['title'];
            $this->description = $dataPage['description'];
            $this->keywords = $dataPage['keywords'];
            $categories = $this->M_Category->getCategories();
            $template = $this->M_Templates->getTemplateId($dataPage['id_template'])['full_template_name'];
            $comments = $this->M_Page->getComments($dataPage['id_article']);

            if($dataPage['id_menu'] != 0){
                $this->left_menu = $this->M_Menu->getMenuWithPages($dataPage['id_menu']);
            }else{
                $this->left_menu = null;
            }

           if($_POST['send_comment']){
                if($this->M_Page->comment_form($_POST)){
                    $this->message = "Комментарий отправлен на модерирование!";
                }else{
                    $this->message = "Произошла ошибка!";
                }
            }

            $this->content = $this->Template("v/$template.php", ['page' => $dataPage, 'keywords' => $this->keywords, 'description' => $this->description, 'gallery' => $this->request('widgets/gallery/1'), 'message' => $this->message, 'categories' => $categories, 'comments' => $comments, 'left_menu' => $this->left_menu]);
        }

        public function action_blog($url){
            $M_Articles = new M_Articles();
            $dataPage = $this->M_Pages->getByUrl($url);
            $dataPage['content'] = $M_Articles->getArticles();
            $this->title = $dataPage['title'];
            $this->description = $dataPage['description'];
            $this->keywords = $dataPage['keywords'];
            $categories = $this->M_Category->getCategories();
            $template = $this->M_Templates->getTemplateId($dataPage['id_template'])['full_template_name'];

            if($dataPage['id_menu'] != 0){
                $this->left_menu = $this->M_Menu->getMenuWithPages($dataPage['id_menu']);
            }else{
                $this->left_menu = $this->M_Pages->getByParent($dataPage['id_page']);
            }

            $this->content = $this->Template("v/$template.php", ['page' => $dataPage, 'keywords' => $this->keywords, 'description' => $this->description, 'gallery' => $this->request('widgets/gallery/1'), 'message' => $this->message, 'categories' => $categories]);
        }

		public function action_404(){
		    $this->title = "Ошибка 404";
		    $this->content = $this->Template('v/v_404.php', []);
        }
	}