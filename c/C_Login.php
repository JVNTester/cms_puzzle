<?php
	class C_Login extends C_Base{
		protected $title;
		protected $content;
		protected $M_Users;
		protected $error;

		function __construct(){
			$this->M_Users = new M_Users();
			$this->M_Users->clearUserData();
			$this->error = false;
		}

		protected function action_index(){
			parent::onInput();
			$this->title = 'To Do';
			if($_POST['autorize']){
				$user_id = $this->M_Users->autorize($_POST);
				if($user_id){
					$this->redirect('adarticle');
				}else{
					$this->error = true;
				}
			}

            $this->content = $this->Template('v/v_login.php',array(
                'title'=>$this->title, 'error' => $this->error
            ));
		}
	}