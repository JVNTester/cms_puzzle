<?php

	class C_Pages extends C_Admin_Base{
		protected $title;
		protected $content;
		private $M_Menu;
        private $M_Templates;
		private $M_Pages;
		private $error;

		function __construct(){
            $this->error = false;
            $this->M_Pages = new M_Pages();
            $this->M_Menu = new M_Menu();
            $this->M_Templates = new M_Templates();
            if(!M_Helpers::can_look('EDIT_PAGES')){
                $this->redirect('login');
            }  
		}

		public function action_index(){
            parent::onInput();
            $this->title = 'Страницы сайта';
            $map = $this->M_Pages->make_tree();

            $this->content = $this->Template('v/admin/v_admin_index.php',array(
                'title'=>$this->title, 'map' => $map
            ));
        }

        public function action_add(){
            parent::onInput();
            $this->title = 'Создание страницы';
            $this->content = "content";
            $this->scripts[] = 'ckeditor/ckeditor';
            $this->scripts[] = 'ck_init';
            $fields = ['is_show' => 'on'];
            $map = $this->M_Pages->make_tree();
            $menus = $this->M_Menu->getAllMenus();
            $templates = $this->M_Templates->getTemplates();
            if(isset($_POST['save_data'])){
                $add = $this->M_Pages->add_data($_POST);
                if(!$add){
                    $this->error = true;
                    $fields = $_POST;
                }else{
                    $this->redirect('pages');
                }
            }

            $this->content = $this->Template('v/admin/v_admin_add.php',array(
                'title'=>$this->title, 'content' => $this->content, 'error' => $this->error, 'fields' => $fields,
                'map' => $map, 'menus' => $menus, 'templates' => $templates
            ));
        }

        public function action_edit(){
            parent::onInput();
            $this->title = 'Редактирование страницы';
            $this->content = "content";
            $this->scripts[] = 'ckeditor/ckeditor';
            $this->scripts[] = 'ck_init';
            $this->scripts[] = "jquery-ui-1.10.1.custom.min";
            $this->scripts[] = "sortable";
            $id_page = (int)$this->params[2];
            $fields = $this->M_Pages->getDataPage($id_page);
            $map = $this->M_Pages->make_tree();
            $menus = $this->M_Menu->getAllMenus();
            $children = $this->M_Pages->getByParent($id_page);
            $templates = $this->M_Templates->getTemplates();
            if($_POST){
                if($_POST['remove']){
                    $remove = $this->M_Pages->remove_page($id_page);
                    if($remove){
                        $this->redirect('pages');
                    }
                }else{
                    $add = $this->M_Pages->edit_data($_POST, $id_page);
                    if(!$add){
                        $this->error = true;
                        $fields = $_POST;
                    }else{
                        $this->redirect('pages');
                    }
                }
            }

            $this->content = $this->Template('v/admin/v_admin_edit.php',[
                'title'=>$this->title, 'content' => $this->content, 'error' => $this->error, 'fields' => $fields,
                'map' => $map, 'menus' => $menus, 'children' => $children, 'templates' => $templates
            ]   );
        }
	}