<?php
abstract class C_Admin_Base extends Controller{
	protected $title;
	protected $content;
    protected $needLogin;
    protected $user;
    protected $styles;
    protected $css_plugins;
    protected $js_plugins;
    protected $js_additional;
    protected $scripts;

    protected $M_Users;

	public function __construct(){}

	protected function onInput(){
        $this->M_Users = new M_Users();
        $this->needLogin = true;
        $this->user = $this->M_Users->getUserData();
        if($this->needLogin == true && $this->user[0] == null){
            $this->redirect('login');
        }

        $this->css_plugins = ['fontawesome-free/css/all.min', 'tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min'];
        $this->styles = ['bootstrap', 'admstyle']; //'admstyle'

        $this->js_plugins = [ ]; //'jquery-ui/jquery-ui.min' 'jquery/jquery.min',
        $this->js_additional = [ ]; //'jquery-ui/jquery-ui.min' 'jquery/jquery.min',
        $this->scripts = ['jquery','onload'];//'bootstrap.min', jquery, 'adminlte' 
		$this->title = 'Админка';
		$this->content = "content";

		
	}
	protected function onOutput(){
		$page = $this->Template('v/admin/v_admin_main.php',array(
			'title' => $this->title,'content'=>$this->content,
            'styles' => $this->styles, 'scripts' => $this->scripts, 'css_plugins' => $this->css_plugins,
            'js_plugins' => $this->js_plugins, 'js_additional' => $this->js_additional, 'user' => $this->user[0]
        ));
		echo $page;
	}

}
