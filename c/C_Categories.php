<?php

	class C_Categories extends C_Admin_Base{
		protected $title;
		protected $content;
        private $M_Category;
        private $M_Articles;
		private $error;

		function __construct(){
            $this->error = false;
            $this->M_Category = new M_Category();
            $this->M_Templates = new M_Templates();
            $this->M_Articles = new M_Articles();
            if(!M_Helpers::can_look('ADD_CATEGORY')){
                $this->redirect('login');
            }
		}

		public function action_index(){
            parent::onInput();
            $this->title = 'Категории статей сайта';
            $categories = $this->M_Category->getCategories();

            $this->content = $this->Template('v/admin/categories/v_categories_all.php',array(
                'title'=>$this->title, 'categories' => $categories
            ));
        }

        public function action_add(){
            parent::onInput();
            $this->title = 'Добавление категории';
            $this->content = "content";
            $categories = $this->M_Templates->getTemplates();
            $articles = $this->M_Articles->getArticles();

            if(isset($_POST['save_data'])){
                $add = $this->M_Category->add_category($_POST);
                if(!$add){
                    $this->error = true;
                    $fields = $_POST;
                }else{
                    $this->redirect('categoryes_admin');
                }
            }

            $this->content = $this->Template('v/admin/categories/v_categories_add.php',array(
                'title'=>$this->title, 'content' => $this->content, 'error' => $this->error, 'fields' => $fields, 'categories' => $categories,'articles' => $articles
            ));
        }

        public function action_edit(){
            parent::onInput();
            $this->title = 'Редактирование категории';
            $this->content = "content";
            $id_category = (int)$this->params[2];
            $fields = $this->M_Category->getCategoryById($id_category);
            $articles = $this->M_Articles->getArticles();
            if($_POST){
                if($_POST['save_data']){
                    $add = $this->M_Category->edit_category($_POST, $id_category);
                    if(!$add){
                        $this->error = true;
                        $fields = $_POST;
                    }else{
                        $this->redirect('categoryes_admin');
                    }
                }else{
                    $remove = $this->M_Category->remove_category($id_category);
                    if(!$remove){
                        $this->error = true;
                        $fields = $_POST;
                    }else{
                        $this->redirect('categoryes_admin');
                    }
                }
            }

            $this->content = $this->Template('v/admin/categories/v_categories_edit.php',[
                'title'=>$this->title, 'content' => $this->content, 'error' => $this->error, 'fields' => $fields,
                'articles' => $articles
            ]   );
        }
	}