<?php

class C_Ajax extends Controller{

    public function __construct(){}

    public function action_image(){
        $M_Images = new M_Images();
        $M_Gallery = new M_Gallery();

//        var_dump($_POST);
        $id_image = $M_Images->upload_base64($_POST['name'],$_POST['value']);
        if($id_image){
            $M_Gallery->add_image($_POST['id_gallery'], $id_image);
            die($_POST['name'] . ' - успешно загружено');
        }
        die('Ошибка');
    }

    public function action_galsort(){
        $M_Gallery = new M_Gallery();
        echo (int)$M_Gallery->sort($_POST['id_gallery'], $_POST['images']);
    }

}
