<?php
require_once("m/M_Route.php");
abstract class Controller{
	protected $title;
	protected $content;
	protected $params;

	protected function onInput(){}
	protected function onOutput(){}
	public function Go($action, $params){
	    $this->params = $params;
		$this->onInput();
		$this->$action();
		$this->onOutput();
	}
    public function Template($filename,$args = array()){
        foreach($args as $k => $v){
            $$k = $v;
        }
        ob_start();
        include($filename);
        return ob_get_clean();
    }

    public function __call($name, $params){
        $this->p404();
    }

    public function p404(){
	    $c = new C_Page();
	    $c->Request('action_404', []);
	    die();
    }

	public function redirect($url){
		header('Location:'. BASE_URL .'/'.$url);
		die();
	}
	public function dump($arr){
	    echo '<pre>';
	    print_r($arr);
        echo '</pre>';
    }

    protected function request($url){
        ob_start();

        if(strpos($url, 'http://') === 0 || strpos($url, 'https://')){
            echo file_get_contents($url);
        }else{
            $route = new M_Route($url);
            $route->Request();
        }

        return ob_get_clean();
    }

}