<?php


class C_Widgets extends Controller{
    protected $content;
    private $M_Gallery;

    public function __construct(){
        $this->content = "";
    }

    public function onOutput(){
        echo $this->content;
    }

    public function action_gallery(){
        $this->M_Gallery = new M_Gallery();
        $id_gallery = (int)$this->params[2] ? (int)$this->params[2] : 0;
        $gallery = $this->M_Gallery->getImages($id_gallery);
        $this->content = $this->Template('v/widgets/v_gallery.php', [
            'gallery' => $gallery
        ]);
    }

}