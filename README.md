# cms

This product is based on the MVC pattern.
This pattern involves dividing the program code into 3 logical components: Controller (the logic of the page), Model (functional component), View (visual design of the page).
According to the selected pattern, the main blocks implemented in this project can be distinguished:
In the implemented system, the process of forming a page consists of several stages. As an example, consider the process of forming the main page of a site:
.htaccess redirects all requests to the index.php file, passing all the data following the site domain as GET parameters;
Index.php creates an instance of the M_Route class in which the page controller is defined, which must be formed according to the GET parameters;
M_Route, defining the controller creates an instance of the C_Page class. Next, the C_Page class is created, in which the page data is received from the database, the necessary templates are connected, and the page is displayed in the browser.

To work with the database, the PHP PDO driver is used, which allows you to use "prepared" queries when accessing the database. This allows you to increase the security of working with the database and avoid undesirable consequences.
Among the functions of the program can be identified:
- Formation of content pages on the user side;
- Work with content pages on the administrator side;
- Work with menus for pages;
- Work with articles on the side of the user and administrator;
- Work with categories for articles;
- Work with templates for pages;
- Work with galleries;
- User authorization;
- Reporting;
- Search and filtering;
- Implementation of the separation of user rights;
- Implementation of the widget system.

For the program to work correctly, you must:
- Using a local or virtual server using Apache_2.4-PHP-7.0 and higher;
- The presence of the connection, as well as the created MySQL MariaDB-10.3 MySQL database and above;
- A browser that supports session functions, the ability to use cookies;
Libraries: jquery version 3.3.1 and higher, OwlCarousel version 2.3.4, bootstrap version 4.1.2 and higher.

Program errors:
Form processing. If an error occurs during the processing of the form, the message “fill in all fields” appears;
Error executing PHP code. In the config.php file, the error_reporting function is called, where the output of program errors is disabled. To obtain this data, you must disable this feature;
Error working with the database. If errors occur while working with the database, several situations are possible. The first thing to check is the availability of the created database, the necessary tables and values. Secondly, the data for connecting to the database in the config_db.php file. If other errors occur, the probability of incorrectly entering values ​​in the form fields is high.

The functionality of the program is divided into two logical components - the user part, which any visitor to the site will see, and the admin panel, which is intended for project managers.
To get to the admin panel, you must go to the address of the domain_site / login.
The admin panel allows you to fill the site with content. Consider the main sections.
Pages. This section of the admin panel is designed to create content pages on the site. An example of a content page is the home page, contacts, company information, etc. The section interface is shown in Figure 4.
To create a content page for a site, go to the “Pages” section in the admin panel, then select “Create a new page”. To create a new page, you must specify fields such as:
Section. This field allows you to create hierarchical subpages;
Page address. This field allows you to specify the url of the page that will be displayed when the page is received on the user side;
The title in the menu. This field allows you to define the name of the item in the menu;
Page title;
Key fields and page description are a group of fields for SEO optimization;
Template selection;
Selecting a menu allows you to define the side menu that will be displayed on the page;
Blog Page This field defines the type of page content. If the field is filled, then instead of text content, the page will display a list of articles on the site;
Content

Page display
Articles. This section of the admin panel is designed to create articles.
To create an article, go to the "Articles" section in the admin panel, then the "Create a new article" item. To create a new article, you must specify fields such as:
Page address. This field allows you to specify the url of the page that will be displayed when the page is received on the user side;
The title in the menu. This field allows you to define the name of the item in the menu;
Page title;
The author of the article;
Description of the article to be used in the derivation of the general conclusion of the articles;
Image of the article;
The category display determines the output of the list of categories in the side column of the article;
Template selection;
Selecting a menu allows you to define the side menu that will be displayed on the page;
Blog Page This field defines the type of page content. If the field is filled, then instead of text content, the page will display a list of articles on the site;
Content
Page display;

Menu. This section allows you to create menus for site pages. Created menus can be displayed in the side columns of articles and pages.
To create a menu, go to the "Menu" section in the admin panel, then the "Create a new menu" item. To create a new menu, you must specify fields such as:
Menu name;
List of pages that enter the menu.
The menu section allows you to edit the display order of items in the menu. To do this, select the “sorting” item in the menu section when displaying a general list of created menus.

Categories. This section of the admin panel is designed to create categories of articles.
To create a category, go to the "Categories" section in the admin panel, then the "Create a new category" item. To create a new category, you must specify fields such as:
Name of category;
List of pages that fall into the category.

Templates. This section of the admin panel is designed to create page templates.
To create a page template, go to the "Templates" section in the admin panel, then select "Create a new template". To create a new template, you must specify fields such as:
The full name of the template, which is the name of the file without the extension;
The name of the template, which will be reflected in the general list of templates.

Galleries. This section of the admin panel is for creating galleries.
To create a gallery, go to the "Galleries" section in the admin panel, then select "Create a new gallery". To create a new gallery, you must specify fields such as:
Name in the system;
The name that will be used for general display in the gallery list.
After this procedure, you will be redirected to the page for uploading images to the gallery.
To upload images to the gallery, just transfer the files to the marked area. After loading the images, it is possible to edit the list of images, the output sequence of the gallery images.

Users This section of the admin panel is designed to create new users and edit existing ones.
To create a new user, go to the "Users" section in the admin panel, then the "Create a new user" item. To create a new user, you must specify fields such as:
Username;
Password;
Role. The role will affect the functionality available to the user.

Reports. This section of the admin panel is designed to view reports on the site. In total, at the moment you can study three reports:
1) Statistics of article views for the year
2) Statistics of adding articles for the year
3) Statistics of user visits for the year
