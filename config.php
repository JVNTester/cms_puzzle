<?php
// error_reporting(E_ERROR | E_WARNING);

function __autoload($classname){
    if($classname[0] == 'C'){
        include_once("c/$classname.php");
    }else{
        include_once("m/$classname.php");
    }
}

define('CSS_DIR', 'v/css');
define('PLUGIN_DIR', 'v/plugins');
define('JS_DIR', 'v/js');
define('BASE_URL', '');
define('ALL_IMG_DIR', 'v/img/');
define('ARTICLES_IMG_DIR', 'v/img/articles/');
define('IMG_DIR', 'v/img/gallery/img_big/');
define('IMG_SMALL_DIR', 'v/img/gallery/img_small/');
define('IMG_SMALL_WIDTH', 200);