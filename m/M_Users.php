<?php

require_once("RyF.php");
class M_Users{
    private $RyF;
    private $pdo;

    public function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    private function checkName($username){
        $sql = $this->pdo->prepare("SELECT * FROM `users` WHERE `username`=:username");
        $sql->execute(['username' => $username]);
        return $sql->fetchAll();
    }


    public function addUser($arr){
        $username = trim($arr['username']);
        $password = trim($arr['password']);
        $id_role = (int)$arr['id_role'];

        if($username && $password && $id_role){
            $password = md5(md5($password));
            if(!$this->checkName($username)){
                $sql = $this->pdo->prepare("INSERT INTO `users` (`username`, `password`, `id_role`) VALUES (:username, :password, :id_role)");
                $sql->execute(['username' => $username, 'password' => $password, 'id_role' => $id_role]);
                $id_user = $this->pdo->lastInsertId();
                return $id_user;
            }else{
                return false;
            }
        }
        return false;
    }

    public function updateUser($id_user, $arr){
        $username = trim($arr['username']);
        $password = trim($arr['password']);
        $id_role = (int)$arr['id_role'];
        if($username && $id_role){
            if($password){
                $password = md5(md5($password));
                $sql = $this->pdo->prepare("UPDATE `users` SET `username`=:username, `password`=:password, `id_role`=:id_role WHERE `id_user`=:id_user");
                $sql->execute(['username' => $username,'id_role' => $id_role, 'id_user' => $id_user, 'password' => $password]);
                return true;
            }else{
                $sql = $this->pdo->prepare("UPDATE `users` SET `username`=:username, `id_role`=:id_role WHERE `id_user`=:id_user");
                $sql->execute(['username' => $username,'id_role' => $id_role, 'id_user' => $id_user]);
                return true;
            }
        }
        return false;
    }

    public function deleteUser($id_user){
        $sql = $this->pdo->prepare("DELETE FROM `users` WHERE `id_user`=:id_user");
        $sql->execute(['id_user' => $id_user]);
        return true;
    }



    public function getRoles(){
        $sql = $this->pdo->prepare("SELECT * FROM `roles`");
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getUserData(){
	    $username = $_SESSION['username'];
	    if(!$username && $_COOKIE['username']){
            $username = $_COOKIE['username'];
        }
        $sql = $this->pdo->prepare("SELECT * FROM `users` JOIN roles USING(`id_role`) WHERE `username`=:username");
        $sql->execute(['username' => $username]);
        $userData = $sql->fetchAll();
	    return $userData;
    }

    public function getAllUsers(){
	    $sql = $this->pdo->prepare("SELECT *, roles.name as rname FROM `users` JOIN `roles` USING(`id_role`)");
	    $sql->execute();
	    return $sql->fetchAll();
    }

    public function getUserDataById($id_user){
        $sql = $this->pdo->prepare("SELECT * FROM `users` JOIN roles USING(`id_role`) WHERE `id_user`=:id_user");
        $sql->execute(['id_user' => $id_user]);
        return $sql->fetchAll();
    }


    public function getFullUserData(){
        $username = $this->getUserData()[0]['username'];
        $sql = $this->pdo->prepare("SELECT * FROM `users` WHERE `username`='$username'");
        $sql->execute();
        return $sql->fetchAll();

    }

    public function getPrivs(){
        $dataUser = $this->getFullUserData()[0];
        if(!$dataUser){
            return [];
        }

        $sql = $this->pdo->prepare("SELECT privs.name as name FROM `privs2role` LEFT JOIN privs USING(id_priv)
                                             WHERE `id_role`=:id_role");
        $sql->execute(['id_role' => $dataUser['id_role']]);
        $data = $sql->fetchAll();

        $privs = [];

        foreach($data as $one){
            $privs[] = $one['name'];
        }

        return $privs;
    }


    public function clearUserData(){
        unset($_SESSION['username']);
        setcookie('username','',time() - 1);
    }

    public function autorize($arr){
        $username = trim($arr['username']);
        $password = trim($arr['password']);

        if($username && $password){
            $password = md5(md5($password));
            $check = $this->RyF->Select('users')
                                ->where(['username' => $username, 'password' => $password])
                                ->execute();
            if($check){
                $_SESSION['username'] = $username;
                setcookie('username', $username, time() + 3600);
                return $check[0]['id_user'];
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

}