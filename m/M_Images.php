<?php

require_once("RyF.php");
class M_Images{
    private $RyF;
    private $pdo;

    public function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    public function upload_base64($name, $value){
        if(!$this->check_type($name))
            return false;

        $getMime = explode('.', $name);
        $mime = strtolower(end($getMime));
        $filename = mt_rand(0, 10000000) . '.' . $mime;

        while(file_exists(IMG_DIR . $filename))
            $filename = mt_rand(0, 10000000) . '.' . $mime;

        $sql = $this->pdo->prepare("INSERT INTO `images` (`path`) VALUES (:path)");
        $sql->execute(['path' => $filename]);
        $id = $this->pdo->lastInsertId();

//        $id = $this->db->Insert('images', array('path' => $filename));
        $this->move_upload_base64($value, $filename);
        return $id;
    }
    public function delete($id_image)
    {
        $id_image = (int)$id_image;
//        $one = $this->db->Select("SELECT * FROM images WHERE id_image='$id_image'");
        $sql = $this->pdo->prepare("SELECT * FROM `images` WHERE `id_image`=:id_image");
        $sql->execute(['id_image' => $id_image]);
        $one = $sql->fetchAll();

//        $this->db->Delete('images', "id_image='$id_image'");
        $sql = $this->pdo->prepare("DELETE FROM `images` WHERE `id_image`=:id_image");
        $sql->execute(['id_image' => $id_image]);

        $filename = $one[0]['path'];

        if(file_exists(IMG_DIR . $filename))
            unlink(IMG_DIR . $filename);

        if(file_exists(IMG_SMALL_DIR . $filename))
            unlink(IMG_SMALL_DIR . $filename);

        return true;
    }

    private function check_type($name){
        $getMime = explode('.', $name);
        $mime = strtolower(end($getMime));
        $types = array('jpg', 'png', 'gif', 'bmp', 'jpeg');
        return in_array($mime, $types);
    }

    private function move_upload_base64($file, $name)
    {
        $data = explode(',', $file);

        $encodedData = str_replace(' ','+',$data[1]);
        $decodedData = base64_decode($encodedData);


        if(file_put_contents(IMG_DIR . $name, $decodedData)){
            $this->resize(IMG_DIR . $name, IMG_SMALL_DIR . $name, IMG_SMALL_WIDTH);
            return true;
        }

        return false;
    }

    private function resize($src, $dest, $width, $height = null, $rgb = 0xFFFFFF, $quality = 100)
    {
        if (!file_exists($src)) return false;

        $size = getimagesize($src);

        if ($size === false) return false;

        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
        $icfunc = "imagecreatefrom" . $format;
        if (!function_exists($icfunc)) return false;

        $x_ratio = $width / $size[0];

        if($height === null)
            $height = $size[1] * $x_ratio;

        $y_ratio = $height / $size[1];

        $ratio       = min($x_ratio, $y_ratio);
        $use_x_ratio = ($x_ratio == $ratio);

        $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
        $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
        $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
        $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

        $isrc = $icfunc($src);
        $idest = imagecreatetruecolor($width, $height);

        imagefill($idest, 0, 0, $rgb);
        imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0,
            $new_width, $new_height, $size[0], $size[1]);

        imagejpeg($idest, $dest, $quality);

        imagedestroy($isrc);
        imagedestroy($idest);

        return true;
    }
}