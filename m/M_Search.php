<?php

require_once('RyF.php');

class M_Search{
    private $RyF;
    private $pdo;
    private $M_Articles;
    private $M_Category;
    private $M_Pages;

    function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
        $this->M_Articles = new M_Articles();   
        $this->M_Pages = new M_Pages();  
        $this->M_Category = new M_Category();  
    }

    public function checkCategory($titleCategory){
        $categoryId = $this->M_Category->getyByName($titleCategory)['id_category'];
        return $this->filter(['category' => $categoryId]);
    }

    private function updateDate($date){
        if($date){
            $parts = explode('/', $date);
            $day = $parts[1];
            $month = $parts[0];
            $year = $parts[2];
            $new_date = "$year-$month-$day";
            return $new_date;
        }
        return false;
    }

    public function filter($arr){
        $title = trim($arr['title']);
        $category = (int)$arr['category'];
        $dateFrom = $this->updateDate($arr['dateFrom']);
        $dateTo = $this->updateDate($arr['dateTo']);
        $res = [];
        if($title || $category || $dateFrom || $dateTo){
            $sql = "";
            $executeArr = [];
            if($title && $category){
                $sql = "SELECT * FROM `articles2categories` JOIN articles USING(`id_article`) WHERE `title` LIKE ? AND `id_category`= ? AND `is_show`='1'";
                $executeArr[] = "%$title%";
                $executeArr[] = $category;
            }
            if($title && !$category){
                $sql = "SELECT * FROM `articles` WHERE `title` LIKE ? AND `is_show`='1'";
                $executeArr[] = "%$title%";
            }
            if($category && !$title){
                $sql = "SELECT * FROM `articles2categories` JOIN articles USING(`id_article`) WHERE `id_category`= ?";
                $executeArr[] = $category;
            }
            if($dateFrom && !$dateTo){
                if(!$sql){
                    $sql = "SELECT * FROM `articles` WHERE `date` > STR_TO_DATE(?, '%Y-%m-%d')";
                }else{
                    $sql .= " AND `date` > STR_TO_DATE(?, '%Y-%m-%d')";
                }
                $executeArr[] = $dateFrom;
            }
            if($dateTo && !$dateFrom){
                if(!$sql){
                    $sql = "SELECT * FROM `articles` WHERE `date` < STR_TO_DATE(?, '%Y-%m-%d')";
                }else{
                    $sql .= " AND `date` < STR_TO_DATE(?, '%Y-%m-%d')";
                }
                $executeArr[] = $dateTo;
            }
            if($dateTo && $dateFrom){
                if(!$sql){
                    $sql = "SELECT * FROM `articles` WHERE `date` BETWEEN STR_TO_DATE(?, '%Y-%m-%d') AND STR_TO_DATE(?, '%Y-%m-%d')";
                }else{
                    $sql .= " AND `date` BETWEEN STR_TO_DATE(?, '%Y-%m-%d') AND STR_TO_DATE(?, '%Y-%m-%d')";
                }
                $executeArr[] = $dateFrom;
                $executeArr[] = $dateTo;
            }

            $dataSql = $this->pdo->prepare($sql);
            $dataSql->execute($executeArr);
            return $dataSql->fetchAll();
        }
        return false;
    }
    
}