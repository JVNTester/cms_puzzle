<?php

require_once('RyF.php');
require_once('M_Images.php');
class M_Gallery{
    private $RyF;
    private $pdo;

    public function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    public function getAllGallery(){
        $sql = $this->pdo->prepare("SELECT * FROM `gallery`");
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getGallery($id_gallery){
        $sql = $this->pdo->prepare("SELECT * FROM `gallery` WHERE `id_gallery`=:id_gallery");
        $sql->execute(['id_gallery' => $id_gallery]);
        return $sql->fetchAll()[0];
    }

    public function getImages($id_gallery){
        $sql = $this->pdo->prepare("SELECT * FROM gallery_images LEFT JOIN images USING(id_image) WHERE `id_gallery`=:id_gallery ORDER BY `num_sort` ASC");
        $sql->execute(['id_gallery' => $id_gallery]);
        return $sql->fetchAll();
    }

    public function addGallery($arr){
        $title = trim($arr['title']);
        $name = trim($arr['name']);

        if($title && $name){
            $sql = $this->pdo->prepare("INSERT INTO `gallery` (`title_gallery`, `name`) VALUES (:title, :name)");
            $sql->execute([ 'title' => $title, 'name' => $name ]);
            return $this->pdo->lastInsertId();
        }else{
            return false;
        }
    }

    public function delete_gallery($id_gallery){
        $sql = $this->pdo->prepare("DELETE FROM `gallery` WHERE `id_gallery` = :id_gallery");
        $sql->execute([ 'id_gallery' => $id_gallery ]);

        $sql = $this->pdo->prepare("DELETE FROM `gallery_images` WHERE `id_gallery` = :id_gallery");
        $sql->execute([ 'id_gallery' => $id_gallery ]);        

        return true;
    }

    public function add_image($id_gallery, $id_image){
        $sql = $this->pdo->prepare("INSERT INTO `gallery_images` (`id_gallery`, `id_image`) VALUES (:id_gallery, :id_image)");
        $sql->execute(['id_gallery' => $id_gallery, 'id_image' => $id_image]);
        return $this->pdo->lastInsertId();
    }

    public function sort($id_gallery, $images){
        $id_gallery = (int)$id_gallery;
        for($i = 0; $i < count($images); $i++){
            $sql = $this->pdo->prepare("UPDATE `gallery_images` SET `num_sort`=:num_sort 
                                                 WHERE `id_gallery`=:id_gallery AND `id_image`=:id_image");
            $sql->execute(['num_sort' => $i, 'id_gallery' => $id_gallery, 'id_image' => $images[$i]]);
        }
        return true;
    }

    public function delete_image($id_gallery, $id_image){
        $sql = $this->pdo->prepare("DELETE FROM `gallery_images` WHERE `id_image`=:id_image AND `id_gallery`=:id_gallery");
        $sql->execute(['id_image' => $id_image, 'id_gallery' => $id_gallery]);
        $M_Images = new M_Images();
        return $M_Images->delete($id_image);
    }
}