<?php

require_once('RyF.php');

class M_Report{
    private $RyF;
    private $pdo;

    function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    public function check_online(){
        $ip = $_SERVER['REMOTE_ADDR'];
        $now = time();
        $sql = $this->pdo->prepare("SELECT * FROM `online` WHERE `ip`= ? AND `date_add` = ?");
        $sql->execute([ $ip, date('Y-m-d', $now) ]);
        $check = $sql->fetchAll();
        
        if(!$check){
            $sql = $this->pdo->prepare("INSERT INTO `online` (`last_visit`, `ip`, `date_add`) VALUES ( ?, ?, ? )");
            $sql->execute([ $now, $ip, date('Y-m-d', $now) ]);
        }

        $sql = $this->pdo->prepare("SELECT * FROM `online` WHERE `date_add` = ?");
        $sql->execute([date('Y-m-d', $now)]);
        $online_today = $sql->fetchAll();
    }

    public function get_online_in_year(){
        $prevYear = date('Y', time() - 60 * 60 * 24 * 365);
        $prevMonth = date('m', time() - 60 * 60 * 24 * 365);
        $arrMonth = [];
        $startMonth = (int)$prevMonth;

        for($i = 0; $i < 13; $i++){
            // 2020-01-01
            $dateCheck = date('Y-m', strtotime("$prevYear-$startMonth-01"));
            $sql = $this->pdo->prepare("SELECT * FROM `online` WHERE YEAR(`date_add`) = YEAR(?) AND MONTH(`date_add`) = MONTH(?)");
            $sql->execute([ date('Y-m-d', strtotime("$prevYear-$startMonth-01")) , date('Y-m-d', strtotime("$prevYear-$startMonth-01")) ]);
            $check = $sql->fetchAll();
            $arrMonth[$dateCheck] = count($check);

            if($startMonth < 12){
                $startMonth += 1;
            }else{
                $startMonth = 1;
                $prevYear = (int)$prevYear + 1;
            }
        }

        return $arrMonth;
    }

    public function get_count_watch_articles(){
        $sql = $this->pdo->prepare("SELECT `count_watch`, `title` FROM `articles` ORDER BY `count_watch` ASC");
        $sql->execute();
        $check = $sql->fetchAll();
        $resArr = [];
        foreach($check as $article){
            $resArr[$article['title']] = $article['count_watch'];
        }
        return $resArr;
    }

    public function get_count_add_articles(){
        $prevYear = date('Y', time() - 60 * 60 * 24 * 365);
        $prevMonth = date('m', time() - 60 * 60 * 24 * 365);
        $arrMonth = [];
        $startMonth = (int)$prevMonth;

        for($i = 0; $i < 13; $i++){
            $dateCheck = date('Y-m', strtotime("$prevYear-$startMonth-01"));
            $sql = $this->pdo->prepare("SELECT * FROM `articles` WHERE YEAR(`date`) = YEAR(?) AND MONTH(`date`) = MONTH(?)");
            $sql->execute([ date('Y-m-d', strtotime("$prevYear-$startMonth-01")) , date('Y-m-d', strtotime("$prevYear-$startMonth-01")) ]);
            $check = $sql->fetchAll();
            $arrMonth[$dateCheck] = count($check);

            if($startMonth < 12){
                $startMonth += 1;
            }else{
                $startMonth = 1;
                $prevYear = (int)$prevYear + 1;
            }
        }

        return $arrMonth;
    }

    
}