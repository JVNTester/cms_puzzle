<?php
require_once('RyF.php');

class M_Articles{
    private $RyF;
    private $pdo;

    function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    public function addCount($id_article){
        // check user
        $ip_user = $_SERVER['REMOTE_ADDR'];
        $sql = $this->pdo->prepare("SELECT * FROM `users2articles` WHERE `id_article`=:id_article AND `ip_user` = :ip_user");
        $sql->execute(['id_article' => $id_article, 'ip_user' => $ip_user]);
        $check_user = $sql->fetchAll();

        if(!$check_user){
            // update count
            $sql = $this->pdo->prepare("SELECT * FROM `articles` WHERE `id_article`=:id_article");
            $sql->execute(['id_article' => $id_article]);
            $now_watch = $sql->fetchAll()[0]['count_watch'];

            $sql = $this->pdo->prepare("UPDATE `articles` SET `count_watch` = :count_watch WHERE `id_article`=:id_article");
            $sql->execute(['id_article' => $id_article, 'count_watch' => $now_watch + 1]);

            $sql = $this->pdo->prepare("INSERT INTO `users2articles` (`ip_user`, `id_article`) VALUES (:ip_user, :id_article)");
            $sql->execute(['id_article' => $id_article, 'ip_user' => $ip_user]);
            return true;
        }
        return true;
    }

    public function getArticles($is_admin = false){
        if($is_admin){
            $sql = $this->pdo->prepare("SELECT * FROM `articles` ORDER BY `date` DESC");
        }else{
            $sql = $this->pdo->prepare("SELECT * FROM `articles` WHERE `is_show`='1' ORDER BY `date` DESC");
        }
        $sql->execute();
        $articles = $sql->fetchAll();
        return $articles;
    }
    public function getArticlesByTitle($title){
        $title = trim($title);
        $sql = $this->pdo->prepare("SELECT * FROM `articles` WHERE `title`=:title AND `is_show`='1'");
        $sql->execute(['title' => $title]);
        $articles = $sql->fetchAll();
        return $articles;
    }
    public function getArticlesInCategory($id_page){
        $sql = $this->pdo->prepare("SELECT * FROM `pages` WHERE `id_page`=:id_page");
        $sql->execute(['id_page' => $id_page]);
        $articles = $sql->fetchAll();
        return $articles;
    }
    public function getCategories(){
        $sql = $this->pdo->prepare("SELECT * FROM `categories`");
        $sql->execute();
        $categories = $sql->fetchAll();
        return $categories;
    }

    public function getArticleByUrl($url){
        $sql = $this->pdo->prepare("SELECT * FROM `articles` WHERE `url`=:url");
        $sql->execute(['url' => $url]);
        $article = $sql->fetchAll();
        return $article[0];
    }

    public function getDataArticle($id_article){
        $sql = $this->pdo->prepare("SELECT * FROM `articles` WHERE `id_article`=:id_article");
        $sql->execute(['id_article' => $id_article]);
        $article = $sql->fetchAll();
        return $article;
    }

    public function add_data($arr){
        $url = trim($arr['url']);
        $autor = trim($arr['autor']);
        $title = trim($arr['title']);
        $content = trim($arr['content']);
        $description = trim($arr['description']);
        $image = trim($arr['image']);
        $id_template = (int)$arr['id_template'];
        $is_show = $arr['is_show'] ? 1 : 0;
        $show_category = $arr['show_category'] ? 1 : 0;
        $id_menu = (int)$arr['id_menu'];
        if($url && $autor && $title && $description && $content && $image) {
            $add = $this->RyF->Insert('articles')
                    ->values(['url' => $url, 'date' => date('Y-m-d'),
                              'title' => $title, 'autor' => $autor, 'id_template' => $id_template,
                              'description' => $description, 'content' => $content, 'image' => $image, 'is_show' => $is_show,
                              'id_menu' => $id_menu, 'show_category' => $show_category])
                    ->execute();

            if($add){
                return $add;
            }else{
                return false;
            }
        }
    }

    public function edit_data($arr, $id_page){
        $id_page = (int)$id_page;
        $url = trim($arr['url']);
        $autor = trim($arr['autor']);
        $title = trim($arr['title']);
        $content = trim($arr['content']);
        $description = trim($arr['description']);
        $image = trim($arr['image']);
        $id_template = (int)$arr['id_template'];
        $is_show = $arr['is_show'] ? 1 : 0;
        $show_category = $arr['show_category'] ? 1 : 0;
        $id_menu = (int)$arr['id_menu'];
        if($url && $autor && $title && $description && $content && $image) {
            if($arr['save_data']){
                $add = $this->RyF->Update('articles')
                        ->values(['url' => $url, 'date' => date('Y-m-d'),
                              'title' => $title, 'autor' => $autor, 'id_template' => $id_template,
                              'description' => $description, 'content' => $content, 'image' => $image, 'is_show' => $is_show,
                              'id_menu' => $id_menu, 'show_category' => $show_category])
                        ->where(['id_article' => $id_page])
                        ->execute();
                if($add){
                   return $add;
                }else{
                    return false;
                }
            }
            if($arr['remove_data']){
                $remove = $this->RyF->Delete('articles')
                                    ->where(['id_article' => $id_page])
                                    ->execute();
                if($remove){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }

    private function change_url($id_parent){
        $children = $this->RyF->Select('pages')
            ->where(['id_parent' => $id_parent])
            ->execute();
        $page = [];
        if($children){
            foreach($children as $child){
                $page['full_cache_url'] = $this->make_full_url($child['id_parent'], $child['url']);
                $pdo = $this->RyF->getPDO();
                $sql = $pdo->prepare("UPDATE `pages` SET `full_cache_url`=:full_cache_url WHERE `id_page`=:id_page");
                $sql->execute(['full_cache_url' => $page['full_cache_url'], 'id_page' => $child['id_page']]);
                $this->change_url($child['id_page']);
            }
        }
    }

    private function make_full_url($id_parent, $url){
        if($id_parent == 0){
            return $url;
        }

        $parent = $this->RyF->Select('pages')
                            ->where(['id_page' => $id_parent])
                            ->execute();
        return $parent[0]['full_cache_url'] . '/' . $url;

    }
}