<?php

class M_Route{

    private $controller;
    private $action;
    private $params;

    public function __construct($url){
        $info = explode('/', $url);
        $this->params = array();

        foreach ($info as $v)
        {
            if ($v != '')
                $this->params[] = $v;
        }

        $this->action = 'action_';
        $this->action .= (isset($this->params[1])) ? $this->params[1] : 'index';

        switch($this->params[0]){
            case 'users':
                $this->controller = 'C_Users';
                break;
            case 'search':
                $this->controller = 'C_Search';
                $this->action = 'action_index';
                break;
            case 'widgets':
                $this->controller = 'C_Widgets';
                break;
            case 'login':
                $this->controller = 'C_Login';
                break;
            case "gallery":
                $this->controller = 'C_Gallery';
                break;
            case "pages":
                $this->controller = 'C_Pages';
                break;
            case "ajax":
                $this->controller = 'C_Ajax';
                break;
            case "menu":
                $this->controller = 'C_Menu';
                break;
            case "reports":
                $this->controller = 'C_Reports';
                break;
            case "templates":
                $this->controller = 'C_Templates';
                break;
            case "categoryes_admin":
                $this->controller = 'C_Categories';
                break;
            case "contact_inf":
                $this->controller = 'C_ContactInf';
                break;
            case 'adarticle':
                $this->controller = 'C_Articles';
                break;
            case null: // корень сайта
                $this->controller = 'C_Page';
                $this->action = "action_index";
                break;
            default:
                $this->controller = 'C_Page';
                $this->action = "action_index";
        }

    }

    public function Request(){
        $c = new $this->controller();
        $c->Go($this->action, $this->params);
    }

}