<?php

class M_Helpers{
    private static $privs;

    public function __construct(){
    }

    public function can_look($priv){
        $M_Users = new M_Users();
        if(self::$privs === null){
            self::$privs = $M_Users->getPrivs();
        }
       // var_dump(self::$privs, $priv);
        return ( in_array($priv, self::$privs) || in_array('ALL', self::$privs) );
    }

}