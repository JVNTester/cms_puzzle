<?php
require_once('RyF.php');

class M_Pages{
    private $RyF;
    private $pdo;

    function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    public function make_tree($start_level = 0){
        $map = [];
        $sql = $this->pdo->prepare("SELECT * FROM `pages` WHERE `id_parent`=:start_level 
                                             ORDER BY `children_sort`, `id_page` ASC");
        $sql->execute(['start_level' => $start_level]);
        $pages = $sql->fetchAll();

        if(!empty($pages)){
            foreach($pages as $page){
                $page['children'] = $this->make_tree($page['id_page']);
                $map[] = $page;
            }
        }
        return $map;
    }
    public function getDataChildren($id){
        $page = $this->RyF->Select('pages')
                        ->where(['id_parent' => $id])
                        ->execute();
        return $page;
    }

    public function getByUrl($url){
        $pdo = $this->RyF->getPDO();
        $sql = $pdo->prepare("SELECT * FROM `pages` WHERE `full_cache_url`=:url");
        $sql->execute(['url' => $url]);
        $data = $sql->fetchAll();
        return $data[0];
    }


    public function getDataPage($id){
        $page = $this->RyF->Select('pages')
                        ->where(['id_page' => $id])
                        ->execute();
        return $page[0];
    }

    public function getByParent($id_parent){
        $id_parent = (int)$id_parent;
        $sql = $this->pdo->prepare("SELECT * FROM `pages` WHERE `id_parent`=:id_parent ORDER BY `children_sort` ASC");
        $sql->execute(['id_parent' => $id_parent]);
        return $sql->fetchAll();
    }

    public function add_data($arr){
        $id_parent = (int)$arr['id_parent'];
        $url = trim($arr['url']);
        $title_in_menu = trim($arr['title_in_menu']);
        $title = trim($arr['title']);
        $keywords = trim($arr['keywords']);
        $description = trim($arr['description']);
        $content = trim($arr['content']);
        $is_show = $arr['is_show'] ? 1 : 0;
        $is_blog = $arr['is_blog'] ? 1 : 0;
        $id_menu = (int)$arr['id_menu'];
        $id_template = (int)$arr['id_template'];
        if($url && $title_in_menu && $title && $keywords && $description && $id_menu !== null) {
            if (!$title_in_menu) {
                $title_in_menu = $title;
            }
            $full_cache_url = $this->make_full_url($id_parent, $url);
            $add = $this->RyF->Insert('pages')
                    ->values(['id_parent' => $id_parent, 'url' => $url, 'full_cache_url' => $full_cache_url,
                              'title' => $title, 'title_in_menu' => $title_in_menu, 'keywords' => $keywords,
                              'description' => $description, 'content' => $content, 'is_show' => $is_show,'is_blog' => $is_blog,
                              'id_menu' => $id_menu, 'id_template' => $id_template])
                    ->execute();

            if($add){
                return $add;
            }else{
                return false;
            }
        }
    }

    public function edit_data($arr, $id_page){
        $id_parent = (int)$arr['id_parent'];
        $url = trim($arr['url']);
        $title_in_menu = trim($arr['title_in_menu']);
        $title = trim($arr['title']);
        $keywords = trim($arr['keywords']);
        $description = trim($arr['description']);
        $content = trim($arr['content']);
        $is_show = $arr['is_show'] ? 1 : 0;
        $is_blog = $arr['is_blog'] ? 1 : 0;
        $id_menu = (int)$arr['id_menu'];
        $id_template = (int)$arr['id_template'];
        if($url && $title_in_menu && $title && $keywords && $description && $content && $id_menu !== null) {
            if($id_parent != $id_page){
                if (!$title_in_menu) {
                    $title_in_menu = $title;
                }
                $full_cache_url = $this->make_full_url($id_parent, $url);
                $add = $this->RyF->Update('pages')
                    ->values(['id_parent' => $id_parent, 'url' => $url, 'full_cache_url' => $full_cache_url,
                        'title' => $title, 'title_in_menu' => $title_in_menu, 'keywords' => $keywords,
                        'description' => $description, 'content' => $content, 'is_show' => $is_show, 'is_blog' => $is_blog,
                        'id_menu' => $id_menu, 'children_sort' => '0', 'id_template' => $id_template])
                    ->where(['id_page' => $id_page])
                    ->execute();
                $this->change_url($id_page);


                $pagesArr = explode(',', $arr['pages']);
                for($i = 0; $i < count($pagesArr); $i++){
                    $sql = $this->pdo->prepare("UPDATE `pages` SET `children_sort`=:num_sort WHERE `id_page`=:id_page");
                    $sql->execute(['num_sort' => $i, 'id_page' => $pagesArr[$i]]);
                }

                return true;
            }else{
                return false;
            }
        }
    }

    public function remove_page($id_page){ 
        $map = $this->make_tree($id_page);
        if($map){
            foreach($map as $page){
                $children_page = $page['children'];
                $sql = $this->pdo->prepare("DELETE FROM `pages` WHERE `id_page`=:id_page");
                $sql->execute(['id_page' => $id_page]);
                foreach($children_page as $child){
                    $this->remove_page($child['id_page']);
                }
            }
        }else{
            $sql = $this->pdo->prepare("DELETE FROM `pages` WHERE `id_page`=:id_page");
            $sql->execute(['id_page' => $id_page]);
        }
       return true;
    }

    private function change_url($id_parent){
        $children = $this->RyF->Select('pages')
            ->where(['id_parent' => $id_parent])
            ->execute();
        $page = [];
        if($children){
            foreach($children as $child){
                $page['full_cache_url'] = $this->make_full_url($child['id_parent'], $child['url']);
                $pdo = $this->RyF->getPDO();
                $sql = $pdo->prepare("UPDATE `pages` SET `full_cache_url`=:full_cache_url WHERE `id_page`=:id_page");
                $sql->execute(['full_cache_url' => $page['full_cache_url'], 'id_page' => $child['id_page']]);
                $this->change_url($child['id_page']);
            }
        }

    }

    private function make_full_url($id_parent, $url){
        if($id_parent == 0){
            return $url;
        }

        $parent = $this->RyF->Select('pages')
                            ->where(['id_page' => $id_parent])
                            ->execute();
        return $parent[0]['full_cache_url'] . '/' . $url;

    }
}