<?php

require_once('RyF.php');

class M_Model{
    private $RyF;
    private $pdo;

	function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    public function getContactInf(){
        $sql = $this->pdo->prepare("SELECT * FROM `contact_inf`");
        $sql->execute();
        return $sql->fetchAll()[0];
    }

    public function save_data_inf($arr){
        $phone = trim(htmlspecialchars($arr['phone']));
        $email = trim(htmlspecialchars($arr['email']));
        $location = trim(htmlspecialchars($arr['location']));
        $desc = trim(htmlspecialchars($arr['description']));
        if($phone && $email && $location){
            $sql = $this->pdo->prepare("UPDATE `contact_inf` SET `phone`=:phone, `email`=:email, `location`=:location, `description`=:description");
            $sql->execute(['phone' => $phone, 'email' => $email, 'location' => $location, 'description' => $desc]);
            return true;
        }
    }

}