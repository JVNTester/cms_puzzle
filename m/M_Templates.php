<?php

require_once("RyF.php");
class M_Templates{
    private $RyF;
    private $pdo;

    public function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }
    
    public function getTemplates(){
        $sql = $this->pdo->prepare("SELECT * FROM `templates`");
        $sql->execute();
        $template = $sql->fetchAll();
        return $template;
    }

    public function getTemplateId($id_template){
        $sql = $this->pdo->prepare("SELECT * FROM `templates` WHERE `id_template`=:id_template");
        $sql->execute(['id_template' => $id_template]);
        $template = $sql->fetchAll();
        return $template[0];
    }

    public function add_template($arr){
        $name_template = trim(htmlspecialchars($arr['name_template']));
        $full_template_name = trim(htmlspecialchars($arr['full_template_name']));
        if($name_template && $full_template_name){
            $sql = $this->pdo->prepare("INSERT INTO `templates` (`name_template`, `full_template_name`) VALUES (:name_template,:full_template_name)");
            $sql->execute(['name_template' => $name_template, 'full_template_name' => $full_template_name]);

            return true;
        }else{
            return false;
        }
    }
   public function edit_template($arr, $id_template){
        $name_template = trim(htmlspecialchars($arr['name_template']));
        $full_template_name = trim(htmlspecialchars($arr['full_template_name']));
        if($name_template && $full_template_name){
            $sql = $this->pdo->prepare("UPDATE `templates` SET `name_template`=:name_template, `full_template_name`=:full_template_name WHERE `id_template`=:id_template");
            $sql->execute(['name_template' => $name_template, 'full_template_name' => $full_template_name, 'id_template' => $id_template]);
            return true;
        }else{
            return false;
        }
    }

    public function remove_template($id_template){
        $sql = $this->pdo->prepare("DELETE FROM `templates` WHERE `id_template`=:id_template");
        $sql->execute(['id_template' => $id_template]);
        return true;
    }
}