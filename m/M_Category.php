<?php

require_once('RyF.php');

class M_Category{
    private $RyF;
    private $pdo;

    function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    public function getCategories(){
        $sql = $this->pdo->prepare("SELECT * FROM `categories`");
        $sql->execute();
        $template = $sql->fetchAll();
        return $template;
    }

    public function getArticlesyByCategoryName($category_name){
        $sql = $this->pdo->prepare("SELECT * FROM `categories` WHERE `title`=:category_name");
        $sql->execute(['category_name' => $category_name]);
        $id_category = $sql->fetchAll()[0]['id_category'];

        $sql = $this->pdo->prepare("SELECT * FROM `articles2categories` JOIN `articles` USING(id_article) WHERE `id_category`=:id_category");
        $sql->execute(['id_category' => $id_category]);
        return $sql->fetchAll();   
    }
    public function getyByName($category_name){
        $sql = $this->pdo->prepare("SELECT * FROM `categories` WHERE `title`=:category_name");
        $sql->execute(['category_name' => $category_name]);
        return $sql->fetchAll()[0];
    }
    public function getCategoryById($id_category){
        $sql = $this->pdo->prepare("SELECT `title` FROM `categories` WHERE `id_category`=:id_category");
        $sql->execute(['id_category' => $id_category]);
        $category = $sql->fetchAll();
        
        $sql = $this->pdo->prepare("SELECT `id_article` FROM `articles2categories` WHERE `id_category`=:id_category");
        $sql->execute(['id_category' => $id_category]);
        $tmp = $sql->fetchAll();
        $articles = [];
        foreach($tmp as $one){
            $articles[] = $one['id_article'];
        }
        return [$category, $articles];
    }

    public function add_category($arr){
        $title = $arr['title'];
        $pages = $arr['id_pages'];
        if($title){
            $sql = $this->pdo->prepare("INSERT INTO `categories` (`title`) VALUES (:title)");
            $sql->execute(['title' => $title]);
            $id = $this->pdo->lastInsertId();

            for($i = 0; $i < count($pages); $i++){
                $sql = $this->pdo->prepare("INSERT INTO `articles2categories` (`id_category`, `id_article`) VALUES (:id_category, :id_article)");
                $sql->execute(['id_category' => $id, 'id_article' => $pages[$i]]);
            }
            return true;
        }
    }

    public function edit_category($arr, $id_category){
        $title = $arr['title'];
        $pages = $arr['id_pages'];
        if($title){
            $sql = $this->pdo->prepare("UPDATE `categories` SET `title`=:title WHERE `id_category`=:id_category");
            $sql->execute(['title' => $title, 'id_category' => $id_category]);
            $id = $this->pdo->lastInsertId();

            $sql = $this->pdo->prepare("DELETE FROM `articles2categories` WHERE `id_category`=:id_category");
            $sql->execute(['id_category' => $id_category]);
            for($i = 0; $i < count($pages); $i++){
                $sql = $this->pdo->prepare("INSERT INTO `articles2categories` (`id_category`, `id_article`) VALUES (:id_category, :id_article)");
                $sql->execute(['id_category' => $id_category, 'id_article' => $pages[$i]]);
            }
            return true;
        }
    }

    public function remove_category($id_category){
        $sql = $this->pdo->prepare("DELETE FROM `categories` WHERE `id_category`=:id_category");
        $sql->execute(['id_category' => $id_category]);

        $sql = $this->pdo->prepare("DELETE FROM `articles2categories` WHERE `id_category`=:id_category");
        $sql->execute(['id_category' => $id_category]);

        return true;
    }
}