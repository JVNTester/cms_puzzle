<?php
	
	class RyF{
		public $pdo;
		public static $instance;
		private $prodaction;

		private $type;
		private $sql_query;
		private $where;
		private $values_for_exec;
		function __construct($prodaction = false,$array_option = array()){
			include_once('config_db.php');
			$this->sql_query = "";
			$this->values_for_exec = array();
			$this->prodaction = $prodaction;
			$opt = $this->set_option($this->prodaction);
			$this->pdo = new PDO("mysql:host=localhost;dbname=".DB,User,Pass,$opt);
			$this->pdo->exec("SET CHARSET utf8");
		}
		public static function Instance($prodaction = false,$array_option = array()){
			if(self::$instance == NULL){
				self::$instance = new RyF($prodaction);
			}
			return self::$instance;
		}

		public function Select($table){
			$this->sql_query = "SELECT * FROM `$table` ";
			$this->type = 'select';
			return $this;
		}
		public function Insert($table){
			$this->sql_query = "INSERT INTO `$table` ";
			$this->type = 'insert';
			return $this;
		}
		public function Update($table){
			$this->sql_query = "UPDATE `$table` ";
			$this->type = 'update';
			return $this;
		}
		public function Delete($table){
			$this->sql_query = "DELETE FROM `$table`";
			$this->type = 'delete';
			return $this;
		}
		/** order_by($val, $type)
			set value by, for example, select
			$type = ASC | DESC
		*/
		public function order_by($val, $type){
			$this->sql_query .= "ORDER BY `$val` $type";
			return $this;
		}
		/** where(array('id'=>1))
			set where the sql query
		*/
		public function where($where){
			$vals = array();
			foreach($where as $k => $v){
				$vals[] = "`$k`=:$k";
				$this->values_for_exec[":".$k] = $v;
			}
			$str = implode(' AND ',$vals);
			$this->sql_query .= " WHERE " . $str;
			return $this;
		}

		public function limit($from, $to = NULL){
			$res_str = "";
			if($to == NULL){
				$res_str = $from;
			}else{
				$res_str = $from . "," . $to;
			}
			$this->sql_query .= " LIMIT " . $res_str;
			return $this;
		}
		/** values(array('name'=>'tester'))
			set values to act
		*/
		public function values($arr_val){
			$cols = array();
			$masks = array();
			$val_for_update = array();

			foreach($arr_val as $k => $v){
				$value_mask = explode(' ',$v);
				$value_mask = $value_mask[0];
				$value_key = explode(' ', $k);
				$value_key = $value_key[0];
				$cols[] = "`$value_key`";
				$masks[] = ':'.$value_key;

				$val_for_update[] = "`$value_key`=:$value_key";
				$this->values_for_exec[":$value_key"] = $v;
			}
			if($this->type == "insert"){
				// INSERT INTO `table` (`name`) VALUES (':qwe');
				$cols_all = implode(',',$cols);
				$masks_all = implode(',',$masks);
				$this->sql_query .= "($cols_all) VALUES ($masks_all)";
			}else if($this->type == 'update'){
				// UPDATE `table` SET `name`=':qwe',`content`='some';
				$this->sql_query .= "SET ";
				// for($i = 0; $i < count($cols); $i++){
					$this->sql_query .= implode(',',$val_for_update);
				// }
			}
			
			return $this;
		}
		/** execute()
			run the query
		*/
		public function execute(){
				// var_dump($this->sql_query);
				// var_dump($this->values_for_exec);
				$q = $this->pdo->prepare($this->sql_query);
				$q->execute($this->values_for_exec);

				if($q->errorCode() != PDO::ERR_NONE){
					$info = $q->errorInfo();
					die($info[2]);
				}
				if($this->type == "select"){
					$this->set_default();
					return $q->fetchall();
				}else if($this->type == 'insert'){
					$this->set_default();
					return $this->pdo->lastInsertId();
				}else{
					$this->set_default();
					return true;	
				}
			
		}
		/** set_defaults()
			set_default values
		*/
		private function set_default(){
			$this->type = "";
			$this->sql_query = "";
			$this->where = array();
			$this->values_for_exec = array();
		}

		private function set_option($prodation,$array = array()){
			$opt = array();
			if(!$this->prodaction){
				if($array){
					$opt = $array;
				}else{
					$opt[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
					$opt[PDO::ATTR_DEFAULT_FETCH_MODE] = PDO::FETCH_ASSOC;
				}
			}else{
				if($array){
					$opt = $array;
				}else{
					$opt[PDO::ATTR_DEFAULT_FETCH_MODE] = PDO::FETCH_ASSOC;
				}
			}
			return $opt;
		}

        public function getPDO(){
		    return $this->pdo;
        }

	}