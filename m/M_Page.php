<?php

require_once('RyF.php');

class M_Page{
    private $RyF;
    private $pdo;

    function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    public function contact_form($arr){
        $name = trim(htmlspecialchars($arr['name']));
        $email = trim(htmlspecialchars($arr['email']));
        $message = trim(htmlspecialchars($arr['message']));

        if($name && $email && $message){
             // mail();
            return true;
        }else{
            return false;
        }
    }

    public function comment_form($arr){
        $username = trim(htmlspecialchars($arr['username']));
        $message = trim(htmlspecialchars($arr['message']));
        $id_article = (int)$arr['id_article'];

        if($username && $message){
            $sql = $this->pdo->prepare("INSERT INTO `comments` (`username`, `message`, `id_article`, `date_val`) VALUES (:username, :message, :id_article, :date_val)");
            $sql->execute(['username' => $username, 'message' => $message, 'id_article' => $id_article, 'date_val' => date('Y-m-d')]);
            return true;
        }else{
            return false;
        }
    }

    public function getComments($id_article){
        $id_article = (int)$id_article;
        $sql = $this->pdo->prepare("SELECT * FROM `comments` WHERE `id_article`=:id_article AND `is_moderate`=:is_moderate");
        $sql->execute(['id_article' => $id_article, 'is_moderate' => '1']);
        $comments = $sql->fetchAll();
        return $comments;
    }

    
}