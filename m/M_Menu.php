<?php
    require_once('RyF.php');
class M_Menu{
    private $RyF;
    private $pdo;

    public function __construct(){
        $this->RyF = RyF::Instance();
        $this->pdo = $this->RyF->getPDO();
    }

    public function is_active($url, $url_page){
        if(strpos($url, $url_page) === 0){
            $symb = substr($url, strlen($url_page), 1);
            if((bool)$symb === false || $symb == '/'){

                return true;
            }
        }
        return false;
    }

    public function getTopMenu(){
        $sql = $this->pdo->prepare("SELECT * FROM `pages` WHERE `id_parent`=:id_parent");
        $sql->execute(['id_parent' => 0]);
        $dataMenu = $sql->fetchAll();
        return $dataMenu;
    }

    public function getAllMenus(){
        $sql = $this->pdo->prepare("SELECT * FROM `menu`");
        $sql->execute();
        return $sql->fetchAll();
    }

    public function getMenu($idMenu){

        $resArr = ['pages' => []];
        $sql = $this->pdo->prepare("SELECT * FROM `menu` WHERE `id_menu`=:id_menu");
        $sql->execute(['id_menu' => $idMenu]);
        $arrDataMenu = $sql->fetchAll();

        $sql = $this->pdo->prepare("SELECT * FROM `menu_pages` WHERE `id_menu`=:id_menu");
        $sql->execute(['id_menu' => $idMenu]);
        $arrItemsMenu = $sql->fetchAll();

        foreach($arrItemsMenu as $item){
            $resArr['pages'][] = $item['id_page'];
        }

        $resArr['title'] = $arrDataMenu[0]['title'];

        return $resArr;
    }

    public function getMenuWithPages($id_menu){
        $sql = $this->pdo->prepare("SELECT * FROM menu_pages JOIN pages USING(id_page) WHERE menu_pages.id_menu=:id_menu ORDER BY `num_sort` ASC");
        $sql->execute(['id_menu' => $id_menu]);
        return $sql->fetchAll();
    }

    public function getFirstMenu(){
        $sql = $this->pdo->prepare("SELECT `id_menu` FROM `menu` LIMIT 1");
        $sql->execute();
        return $sql->fetchAll()[0]['id_menu'];
    }

    public function addMenu($arr){
        $title = trim($arr['title']);
        $pages = $arr['pages'];

        if($title && $pages){
            $sql = $this->pdo->prepare("INSERT INTO `menu` (`title`) VALUES (:title)");
            $sql->execute(['title' => $title]);
            $id_menu = $this->pdo->lastInsertId();

            for($i = 0; $i < count($pages); $i++){
                $sql = $this->pdo->prepare('INSERT INTO `menu_pages` (`id_menu`, `id_page`, `num_sort`) VALUES (:id_menu, :id_page, :num_sort)');
                $sql->execute([ 'id_menu' => $id_menu, 'id_page' => $pages[$i], 'num_sort' => $i ]);
            }

            return $id_menu;
        }
    }

    public function editMenu($id_menu, $arr){
        $title = trim($arr['title']);
        $pages = $arr['pages'];

        if($title && $pages){
            $sql = $this->pdo->prepare("UPDATE `menu` SET `title`=:title WHERE `id_menu`=:id_menu");
            $sql->execute(['title' => $title, 'id_menu' => $id_menu]);

            $sql = $this->pdo->prepare("DELETE FROM `menu_pages` WHERE `id_menu`=:id_menu");
            $sql->execute(['id_menu' => $id_menu]);

            for($i = 0; $i < count($pages); $i++){
                $sql = $this->pdo->prepare("INSERT INTO `menu_pages` (`id_menu`,`id_page`, `num_sort`) VALUES (:id_menu, :id_page, :num_sort)");
                $sql->execute([ 'id_menu' => $id_menu, 'id_page' => $pages[$i], 'num_sort' => $i ]);
            }

            return true;
        }else{
            return false;
        }
    }

    public function deleteMenu($id_menu){
        $sql = $this->pdo->prepare("DELETE FROM `menu` WHERE `id_menu` = :id_menu");
        $sql->execute(['id_menu' => $id_menu]);

        $sql = $this->pdo->prepare("DELETE FROM `menu_pages` WHERE `id_menu` = :id_menu");
        $sql->execute(['id_menu' => $id_menu]);

        return true;
    }

    public function sorting($id_menu, $pages){
        $id_menu = (int)$id_menu;
        $pagesArr = explode(',', $pages);
        for($i = 0; $i < count($pagesArr); $i++){
            $sql = $this->pdo->prepare("UPDATE `menu_pages` SET `num_sort`=:num_sort WHERE `id_menu`=:id_menu AND `id_page`=:id_page");
            $sql->execute(['num_sort' => $i, 'id_menu' => $id_menu, 'id_page' => $pagesArr[$i]]);
        }

        return true;
    }

}